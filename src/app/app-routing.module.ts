import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () => import('./modules/back-office/back-office.module').then(mod => mod.BackOfficeModule)
  },
  {
    path: '',
    loadChildren: () => import('./modules/front-office/front-office.module').then(mod => mod.FrontOfficeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
