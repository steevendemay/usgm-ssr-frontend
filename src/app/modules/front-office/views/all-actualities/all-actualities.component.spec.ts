import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllActualitiesComponent } from './all-actualities.component';

describe('AllActualitiesComponent', () => {
  let component: AllActualitiesComponent;
  let fixture: ComponentFixture<AllActualitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllActualitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllActualitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
