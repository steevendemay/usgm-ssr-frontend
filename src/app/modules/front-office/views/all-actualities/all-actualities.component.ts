import { Component, OnInit } from '@angular/core';
import {Actuality} from '../../../shared-usgm/models/Actuality';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class AllActualitiesComponentVM {
  actualities: Actuality[];
  pageId: number = 1;
  nbPages: number;
}

@Component({
  selector: 'app-all-actualities',
  templateUrl: './all-actualities.component.html',
  styleUrls: ['./all-actualities.component.css']
})
export class AllActualitiesComponent implements OnInit {

  vm: AllActualitiesComponentVM = new AllActualitiesComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getActualities();
  }

  getActualities() {
    this.spinnerManager.show();
    this.foManager.getActualities(this.vm.pageId).subscribe((actualitiesPaginate) => {
      this.vm.actualities = actualitiesPaginate.data;
      this.vm.nbPages = actualitiesPaginate.last_page;
      this.spinnerManager.hide();
    });
  }

  handleClickReadMore(id: number) {
    this.router.navigate([routesConstants.foActualityWithoutId, id]);
  }

  handleChangePage(event: number) {
    this.vm.pageId = event;
    this.getActualities();
  }


}
