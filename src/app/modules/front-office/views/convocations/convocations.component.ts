import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {ConvocationPlayer} from '../../../shared-usgm/models/ConvocationPlayer';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class ConvocationsComponentVM {
  convocationPlayer: ConvocationPlayer;
}
@Component({
  selector: 'app-convocations',
  templateUrl: './convocations.component.html',
  styleUrls: ['./convocations.component.css']
})
export class ConvocationsComponent implements OnInit {

  teamId: number;
  vm: ConvocationsComponentVM = new ConvocationsComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private foManager: FrontOfficeManagerService) { }

  ngOnInit(): void {
    this.getConvocationByTeamId();
  }

  getConvocationByTeamId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.teamId = params.id;
      })).pipe(mergeMap(params => {
        return this.foManager.getConvocationByTeam(this.teamId)}
      )).subscribe((convocation) => {
        this.vm.convocationPlayer = convocation;
        this.spinnerManager.hide();
      });
  }


}
