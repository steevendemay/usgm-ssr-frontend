import { Component, OnInit } from '@angular/core';
import {Office} from '../../../shared-usgm/models/Office';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {of} from 'rxjs';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class OfficeComponentVM {
  offices: Office[];
}
@Component({
  selector: 'app-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.css']
})
export class OfficeComponent implements OnInit {

  vm: OfficeComponentVM = new OfficeComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getOffices();
  }

  getOffices() {
    this.spinnerManager.show();
    return this.foManager.getOffices().subscribe((offices) => {
      this.vm.offices = offices;
      this.spinnerManager.hide();
    });
  }

}
