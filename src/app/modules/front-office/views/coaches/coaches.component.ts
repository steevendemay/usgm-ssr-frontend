import { Component, OnInit } from '@angular/core';
import {Coach} from '../../../shared-usgm/models/Coach';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {faBirthdayCake} from '@fortawesome/free-solid-svg-icons';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class CoachesComponentVM {
  coaches: Coach[];
  faBirthdayCake = faBirthdayCake;

}
@Component({
  selector: 'app-coaches',
  templateUrl: './coaches.component.html',
  styleUrls: ['./coaches.component.css']
})
export class CoachesComponent implements OnInit {

  vm: CoachesComponentVM = new CoachesComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getCoaches();
  }

  getCoaches() {
    this.spinnerManager.show();
    return this.foManager.getCoaches().subscribe((coaches) => {
      this.vm.coaches = coaches;
      this.spinnerManager.hide();
    });
  }

}
