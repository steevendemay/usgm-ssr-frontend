import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {Photo} from '../../../shared-usgm/models/Photo';
import {map, mergeMap} from 'rxjs/operators';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class PhotosAlbumComponentVM {
  photos: Photo[];
  showModal: boolean;
  pict;
}
@Component({
  selector: 'app-photos-album',
  templateUrl: './photos-album.component.html',
  styleUrls: ['./photos-album.component.css']
})
export class PhotosAlbumComponent implements OnInit {

  vm: PhotosAlbumComponentVM = new PhotosAlbumComponentVM();
  albumId: number;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private foManager: FrontOfficeManagerService) { }

  ngOnInit(): void {
    this.getPhotosByAlbumId();
  }

  handleClickShow(pic) {
    this.vm.pict = pic;
    this.vm.showModal = true;
  }

  handleClickHide() {
    this.vm.showModal = false;
  }

  getPhotosByAlbumId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.albumId = params.id;
      })).pipe(mergeMap(params => {
        return this.foManager.getPhotosByAlbumId(this.albumId)}
      )).subscribe((photos) => {
        this.vm.photos = photos;
        this.spinnerManager.hide();
      });
  }

}
