import { Component, OnInit } from '@angular/core';
import { faFutbol } from '@fortawesome/free-solid-svg-icons';
import {Match} from '../../../shared-usgm/models/Match';
import {ActivatedRoute, Router} from '@angular/router';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {Scorer} from '../../../shared-usgm/models/Scorer';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class MatchesComponentVM {
  matches: Match[];
  scorers: Scorer[] = [];
  faFutbol = faFutbol;
}
@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {

  teamId: number;
  vm: MatchesComponentVM = new MatchesComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private foManager: FrontOfficeManagerService) { }

  ngOnInit(): void {
    this.getMatchesByTeamId();
  }

  getMatchesByTeamId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.teamId = params.id;
      })).pipe(mergeMap(params => {
        return this.foManager.getMatchesByTeam(this.teamId)}
      )).subscribe((matches) => {
        this.vm.matches = matches;
        this.spinnerManager.hide();
      });
  }

  handleClickSeeScorer(id: number) {
    this.spinnerManager.show();
    this.vm.scorers = [];
    this.foManager.getScorersByMatchId(id).subscribe((scorers) => {
      this.vm.scorers = scorers;
      this.spinnerManager.hide();
    });
  }
}
