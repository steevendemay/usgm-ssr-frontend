import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {FrontOfficeFormManagerService} from '../../services/front-office-form-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class RegisterComponentVM {
  fGroup: FormGroup;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  vm: RegisterComponentVM = new RegisterComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private foFormManager: FrontOfficeFormManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.foFormManager.generateRegisterForm();
  }

  handleSubmitAccess() {
    this.spinnerManager.show();
    this.foManager.register(this.vm.fGroup.value).subscribe(() => {
      this.router.navigate([routesConstants.foActualities]);
      this.spinnerManager.hide();
    });
  }

}
