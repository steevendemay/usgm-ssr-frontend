import { Component, OnInit } from '@angular/core';
import {Scorer} from '../../../shared-usgm/models/Scorer';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {faFutbol} from '@fortawesome/free-solid-svg-icons';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class RankingScorersComponentVM {
  scorers: Scorer[] = [];
  faFutbol = faFutbol;

}
@Component({
  selector: 'app-ranking-scorers',
  templateUrl: './ranking-scorers.component.html',
  styleUrls: ['./ranking-scorers.component.css']
})
export class RankingScorersComponent implements OnInit {

  vm: RankingScorersComponentVM = new RankingScorersComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getRankingScorer();
  }

  getRankingScorer() {
    this.spinnerManager.show();
    this.foManager.getRankingScorer().subscribe((scorers) => {
      this.vm.scorers = scorers;
      this.spinnerManager.hide();
    });
  }

}
