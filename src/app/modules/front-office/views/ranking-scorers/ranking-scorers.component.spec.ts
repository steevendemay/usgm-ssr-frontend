import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingScorersComponent } from './ranking-scorers.component';

describe('RankingScorersComponent', () => {
  let component: RankingScorersComponent;
  let fixture: ComponentFixture<RankingScorersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RankingScorersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RankingScorersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
