import {Component, OnInit} from '@angular/core';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {Player} from '../../../shared-usgm/models/Player';
import {Post} from '../../../shared-usgm/models/enums/post';
import {faBirthdayCake} from '@fortawesome/free-solid-svg-icons';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class PlayersComponentVM {
  goalkeepers: Player[] = [];
  defenders: Player[] = [];
  midfielders: Player[] = [];
  strikers: Player[] = [];
  faBirthdayCake = faBirthdayCake;

}
@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  vm: PlayersComponentVM = new PlayersComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getPlayers();
  }

  getPlayers() {
    this.spinnerManager.show();
    this.foManager.getPlayers().subscribe((players: Player[]) => {
      for (let player of players) {
        if (player.post === Post.GOALKEEPER) {
          this.vm.goalkeepers.push(player);
        } else if (player.post === Post.DEFENDER) {
          this.vm.defenders.push(player);
        } else if (player.post === Post.MIDFIELDER) {
          this.vm.midfielders.push(player);
        } else if(player.post === Post.STRIKER) {
          this.vm.strikers.push(player);
        }
      }
      this.spinnerManager.hide();
    });
  }
}
