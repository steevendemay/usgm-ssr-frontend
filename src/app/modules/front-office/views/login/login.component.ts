import { Component, OnInit } from '@angular/core';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {FrontOfficeFormManagerService} from '../../services/front-office-form-manager.service';
import {FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {JwtManagerService} from '../../../shared-usgm/services/managers/jwt-manager.service';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class LoginComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  vm: LoginComponentVM = new LoginComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private foFormManager: FrontOfficeFormManagerService,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private jwtManager: JwtManagerService) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.foFormManager.generateLoginForm();
  }

  handleSubmitLogin() {
    this.spinnerManager.show();
    this.foManager.login(this.vm.fGroup.value).subscribe((token) => {
      this.jwtManager.storeJwt(token.success.token);
      this.spinnerManager.hide();
      this.router.navigate([routesConstants.bo, routesConstants.boActualities]);
    });
  }
}
