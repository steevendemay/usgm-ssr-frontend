import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {Sponsor} from '../../../shared-usgm/models/Sponsor';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {Actuality} from '../../../shared-usgm/models/Actuality';
import {Router} from '@angular/router';
import {Scorer} from '../../../shared-usgm/models/Scorer';
import {faFutbol} from '@fortawesome/free-solid-svg-icons';
import {DOCUMENT, isPlatformBrowser} from '@angular/common';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class ActualitiesComponentVM {
  sponsors: Sponsor[];
  actualities: Actuality[];
  scorers: Scorer[] = [];
  faFutbol = faFutbol;
}
@Component({
  selector: 'app-actualities',
  templateUrl: './actualities.component.html',
  styleUrls: ['./actualities.component.css']
})
export class ActualitiesComponent implements OnInit {

  vm: ActualitiesComponentVM = new ActualitiesComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              @Inject(DOCUMENT) private document: Document,
              @Inject(PLATFORM_ID) private platformId: any) { }

  ngOnInit(): void {
    this.getSponsors();
    this.getActualities();
    this.getRankingScorer();
  }

  getRankingScorer() {
    this.spinnerManager.show();
    this.foManager.getRankingScorer().subscribe((scorers) => {
      this.vm.scorers = scorers;
      this.spinnerManager.hide();
    });
  }
  getSponsors() {
    this.spinnerManager.show();
    this.foManager.getSponsors().subscribe((sponsors) => {
      this.vm.sponsors = sponsors;
      this.spinnerManager.hide();
    });
  }

  getActualities() {
    this.spinnerManager.show();
    this.foManager.getActualities(1).subscribe((actualitiesPagination) => {
      this.vm.actualities = actualitiesPagination.data;
      this.spinnerManager.hide();
    });
  }

  handleClickReadMore(id: number) {
    this.router.navigate([routesConstants.foActualityWithoutId, id]);
  }

  handleClickSeeAll() {
    this.router.navigate([routesConstants.foAllActualities]);
  }

  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      (<any>window).twttr.widgets.load();
    }
  }

}
