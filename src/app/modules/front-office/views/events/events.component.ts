import { Component, OnInit } from '@angular/core';
import {faClock, faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class EventsComponentVM {
  faMapMarkerAlt = faMapMarkerAlt;
  faClock = faClock;
  events: Event[];
}
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  vm: EventsComponentVM = new EventsComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getEvents();
  }

  getEvents() {
    this.spinnerManager.show();
    this.foManager.getEvents().subscribe((events) => {
      this.vm.events = events;
      this.spinnerManager.hide();
    });
  }

}
