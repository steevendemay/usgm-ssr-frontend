import { Component, OnInit } from '@angular/core';
import {Album} from '../../../shared-usgm/models/Album';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class AlbumsComponentVM {
  albums: Album[];
}

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  vm: AlbumsComponentVM = new AlbumsComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getAlbums();
  }

  getAlbums() {
    this.spinnerManager.show();
    return this.foManager.getAlbums().subscribe((albums) => {
      this.vm.albums = albums;
      this.spinnerManager.hide();
    });
  }

  handleClickSeeAlbum(id: number) {
    this.router.navigate([routesConstants.foPhotosAlbumWithoutId, id]);
  }

}
