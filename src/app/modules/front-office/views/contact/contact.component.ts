import { Component, OnInit } from '@angular/core';
import {faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';
import {Contact} from '../../../shared-usgm/models/Contact';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class ContactComponentVM {
  faMapMarkerAlt = faMapMarkerAlt;
  contacts: Contact[];
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  vm: ContactComponentVM = new ContactComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getContacts();
  }

  getContacts() {
    this.spinnerManager.show();
    this.foManager.getContacts().subscribe((contacts) => {
      this.vm.contacts = contacts;
      this.spinnerManager.hide();
    });
  }

}
