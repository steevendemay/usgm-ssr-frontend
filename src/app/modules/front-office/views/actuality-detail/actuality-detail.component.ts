import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {Actuality} from '../../../shared-usgm/models/Actuality';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class ActualityDetailComponentVM {
  actuality: Actuality;
  picture: any[] = [];
}
@Component({
  selector: 'app-actuality-detail',
  templateUrl: './actuality-detail.component.html',
  styleUrls: ['./actuality-detail.component.css']
})
export class ActualityDetailComponent implements OnInit {

  actualityId: number;
  vm: ActualityDetailComponentVM = new ActualityDetailComponentVM();


  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private foManager: FrontOfficeManagerService) { }

  ngOnInit(): void {
    this.getActualityById();
  }

  getActualityById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.actualityId = params.id;
      })).pipe(mergeMap(params => {
        return this.foManager.getActualityById(this.actualityId)}
      )).subscribe((actuality) => {
        this.vm.actuality = actuality;
        this.spinnerManager.hide();

        if(actuality.picture1){
          this.vm.picture.push(actuality.picture1);
        }
        if(actuality.picture2){
          this.vm.picture.push(actuality.picture2);
        }
        if(actuality.picture3){
          this.vm.picture.push(actuality.picture3);
        }
      });
  }
}
