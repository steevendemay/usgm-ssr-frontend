import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './views/home/home.component';
import {ActualitiesComponent} from './views/actualities/actualities.component';
import {PlayersComponent} from './views/players/players.component';
import {ContactComponent} from './views/contact/contact.component';
import {ConvocationsComponent} from './views/convocations/convocations.component';
import {CoachesComponent} from './views/coaches/coaches.component';
import {OfficeComponent} from './views/office/office.component';
import {EventsComponent} from './views/events/events.component';
import {MatchesComponent} from './views/matches/matches.component';
import {ActualityDetailComponent} from './views/actuality-detail/actuality-detail.component';
import {RankingScorersComponent} from './views/ranking-scorers/ranking-scorers.component';
import {RegisterComponent} from './views/register/register.component';
import {LoginComponent} from './views/login/login.component';
import {AlbumsComponent} from './views/albums/albums.component';
import {PhotosAlbumComponent} from './views/photos-album/photos-album.component';
import {AllActualitiesComponent} from './views/all-actualities/all-actualities.component';
import {routesConstants} from '../shared-usgm/services/constants/routes';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: HomeComponent},
      {path: routesConstants.foActualities, component: ActualitiesComponent},
      {path: routesConstants.foAllActualities, component: AllActualitiesComponent},
      {path: routesConstants.foPlayers, component: PlayersComponent},
      {path: routesConstants.foContact, component: ContactComponent},
      {path: routesConstants.foConvocations, component: ConvocationsComponent},
      {path: routesConstants.foCoaches, component: CoachesComponent},
      {path: routesConstants.foOffices, component: OfficeComponent},
      {path: routesConstants.foEvents, component: EventsComponent},
      {path: routesConstants.foRegister, component: RegisterComponent},
      {path: routesConstants.foLogin, component: LoginComponent},
      {path: routesConstants.foRankingScorers, component: RankingScorersComponent},
      {path: routesConstants.foAlbums, component: AlbumsComponent},
      {path: routesConstants.foPhotosAlbum, component: PhotosAlbumComponent},
      {path: routesConstants.foMatches, component: MatchesComponent},
      {path: routesConstants.foActuality, component: ActualityDetailComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontOfficeRoutingModule { }
