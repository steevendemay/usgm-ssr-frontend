import { Component, OnInit } from '@angular/core';
import {Team} from '../../../shared-usgm/models/Team';
import {FrontOfficeManagerService} from '../../services/front-office-manager.service';
import {JwtManagerService} from '../../../shared-usgm/services/managers/jwt-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {faUser,faPowerOff} from '@fortawesome/free-solid-svg-icons';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class MenuComponentVM {
  teams: Team[];
  nameUser: string;
  faUser = faUser;
  faPowerOff = faPowerOff;

}
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  vm: MenuComponentVM = new MenuComponentVM();

  constructor(private foManager: FrontOfficeManagerService,
              private jwtManager: JwtManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.vm.nameUser = this.jwtManager.getFirstNameUserConnected();
    this.getTeams();
  }

  getTeams() {
    this.foManager.getTeams().subscribe((teams) => {
      this.vm.teams = teams;
    });
  }


  handleClickMember() {
    this.router.navigate([routesConstants.bo, routesConstants.boActualities]);
  }

  handleClickLogout() {
    this.jwtManager.storeJwt(null);
    this.jwtManager.setFirstNameUserConnected(null);
    this.vm.nameUser = null;
    this.router.navigate([routesConstants.foLogin]);
  }

}
