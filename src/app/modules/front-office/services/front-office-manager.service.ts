import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {constants} from '../../shared-usgm/services/constants/constants';
import {Player} from '../../shared-usgm/models/Player';
import {Observable} from 'rxjs';
import {Sponsor} from '../../shared-usgm/models/Sponsor';
import {Team} from '../../shared-usgm/models/Team';
import {Coach} from '../../shared-usgm/models/Coach';
import {Office} from '../../shared-usgm/models/Office';
import {Contact} from '../../shared-usgm/models/Contact';
import {ConvocationPlayer} from '../../shared-usgm/models/ConvocationPlayer';
import {Match} from '../../shared-usgm/models/Match';
import {Actuality} from '../../shared-usgm/models/Actuality';
import {Scorer} from '../../shared-usgm/models/Scorer';
import {User} from '../../shared-usgm/models/User';
import {Album} from '../../shared-usgm/models/Album';
import {Photo} from '../../shared-usgm/models/Photo';
import {ActualitiesPaginate} from '../../shared-usgm/models/ActualitiesPaginate';

@Injectable({
  providedIn: 'root'
})
export class FrontOfficeManagerService {

  constructor(private http: HttpClient) { }

  getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(constants.urlApi + 'players');
  }

  getSponsors(): Observable<Sponsor[]> {
    return this.http.get<Sponsor[]>(constants.urlApi + 'sponsors');
  }

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(constants.urlApi + 'teams');
  }

  getCoaches(): Observable<Coach[]> {
    return this.http.get<Coach[]>(constants.urlApi + 'coaches');
  }


  getOffices(): Observable<Office[]> {
    return this.http.get<Office[]>(constants.urlApi + 'offices');
  }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(constants.urlApi + 'events');
  }

  getContacts(): Observable<Contact[]> {
    return this.http.get<Contact[]>(constants.urlApi + 'contacts');
  }

  getConvocationByTeam(idTeam: number): Observable<ConvocationPlayer> {
    return this.http.get<ConvocationPlayer>(constants.urlApi + `convocationByTeam/${idTeam}`);
  }

  getMatchesByTeam(idTeam: number): Observable<Match[]> {
    return this.http.get<Match[]>(constants.urlApi + `matchesByTeam/${idTeam}`);
  }

  getPhotosByAlbumId(idAlbum: number): Observable<Photo[]> {
    return this.http.get<Photo[]>(constants.urlApi + `photosByAlbum/${idAlbum}`);
  }

  getActualities(pageId: number) {
    return this.http.get<ActualitiesPaginate>(constants.urlApi + `actualities?page=${pageId}`);
  }

  getActualityById(id: number): Observable<Actuality> {
    return this.http.get<Actuality>(constants.urlApi + `actuality/${id}`);
  }

  getScorersByMatchId(id: number): Observable<Scorer[]> {
    return this.http.get<Scorer[]>(constants.urlApi + `scorersByMatch/${id}`);
  }

  getRankingScorer(): Observable<Scorer[]> {
    return this.http.get<Scorer[]>(constants.urlApi + `rankScorers`);
  }

  getAlbums(): Observable<Album[]> {
    return this.http.get<Album[]>(constants.urlApi + `albums`);
  }

  register(user: User) {
    return this.http.post<User>(constants.urlApi + `register`, user );
  }

  login(user: User) {
    return this.http.post<any>(constants.urlApi + `login`, user );
  }


}
