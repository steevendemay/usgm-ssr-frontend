import { Injectable } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FrontOfficeFormManagerService {

  constructor(private fb: FormBuilder) { }

  generateRegisterForm() {
    return this.fb.group({
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'email': ['', [Validators.required]],
      'password': ['', [Validators.required]],
      'c_password': ['', [Validators.required]],
    })
  }

  generateLoginForm() {
    return this.fb.group({
      'email': ['', [Validators.required]],
      'password': ['', [Validators.required]],
    })
  }
}
