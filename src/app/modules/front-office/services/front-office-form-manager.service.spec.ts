import { TestBed } from '@angular/core/testing';

import { FrontOfficeFormManagerService } from './front-office-form-manager.service';

describe('FrontOfficeFormManagerService', () => {
  let service: FrontOfficeFormManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrontOfficeFormManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
