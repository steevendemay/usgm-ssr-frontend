import { TestBed } from '@angular/core/testing';

import { FrontOfficeManagerService } from './front-office-manager.service';

describe('FrontOfficeManagerService', () => {
  let service: FrontOfficeManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrontOfficeManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
