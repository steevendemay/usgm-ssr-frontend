import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './views/home/home.component';
import {FrontOfficeRoutingModule} from './front-office-routing.module';
import { ActualitiesComponent } from './views/actualities/actualities.component';
import {MenuComponent} from './components/menu/menu.component';
import { PlayersComponent } from './views/players/players.component';
import { ContactComponent } from './views/contact/contact.component';
import { ConvocationsComponent } from './views/convocations/convocations.component';
import { CoachesComponent } from './views/coaches/coaches.component';
import { OfficeComponent } from './views/office/office.component';
import { EventsComponent } from './views/events/events.component';
import { MatchesComponent } from './views/matches/matches.component';
import {SharedUsgmModule} from '../shared-usgm/shared-usgm.module';
import { ActualityDetailComponent } from './views/actuality-detail/actuality-detail.component';
import { RegisterComponent } from './views/register/register.component';
import { LoginComponent } from './views/login/login.component';
import { RankingScorersComponent } from './views/ranking-scorers/ranking-scorers.component';
import { RankingsComponent } from './views/rankings/rankings.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BackOfficeRoutingModule} from '../back-office/back-office-routing.module';
import {NgxDatePickerModule} from '@ngx-tiny/date-picker';
import {DlDateTimeDateModule, DlDateTimePickerModule} from 'angular-bootstrap-datetimepicker';
import { AlbumsComponent } from './views/albums/albums.component';
import { PhotosAlbumComponent } from './views/photos-album/photos-album.component';
import { AllActualitiesComponent } from './views/all-actualities/all-actualities.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [HomeComponent, ActualitiesComponent, MenuComponent, PlayersComponent, ContactComponent,
    ConvocationsComponent, CoachesComponent, OfficeComponent, EventsComponent, MatchesComponent,
    ActualityDetailComponent, RegisterComponent, LoginComponent, RankingScorersComponent, RankingsComponent,
    AlbumsComponent, PhotosAlbumComponent, AllActualitiesComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    FrontOfficeRoutingModule,
    SharedUsgmModule,
    HttpClientModule,
  ]
})
export class FrontOfficeModule { }
