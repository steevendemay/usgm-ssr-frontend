import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitleSectionComponent } from './components/title-section/title-section.component';
import { ConvertMonthPipe } from './pipes/convert-month.pipe';
import {AuthGuard} from './guards/auth-guard';
import { PaginationComponent } from './components/pagination/pagination.component';
import { SpinnerComponent } from './components/spinner/spinner.component';


@NgModule({
  declarations: [TitleSectionComponent, ConvertMonthPipe, PaginationComponent, SpinnerComponent],
  imports: [
    CommonModule,
  ],
  providers:[AuthGuard],
  exports: [TitleSectionComponent, ConvertMonthPipe, PaginationComponent, SpinnerComponent]
})
export class SharedUsgmModule { }
