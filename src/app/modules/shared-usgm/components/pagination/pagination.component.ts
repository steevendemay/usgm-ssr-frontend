import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export class PaginationComponentVM {
  nbPages: number[];
  currentPage: number;
}
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  vm: PaginationComponentVM = new PaginationComponentVM();

  @Input()
  set nbPages(value: number) {
    this.vm.nbPages = Array.from({length:value},(v,k)=>k+1);
  }

  @Input()
  set currentPage(value: number) {
    this.vm.currentPage = value;
  }

  @Output()
  changePage: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  handleClickChangePage(page: number) {
    this.vm.currentPage = page;
    this.changePage.emit(this.vm.currentPage);
  }

  handleClickPrevious() {
    this.vm.currentPage = this.vm.currentPage - 1;
    this.changePage.emit(this.vm.currentPage);
  }

  handleClickNext() {
    this.vm.currentPage = this.vm.currentPage + 1;
    this.changePage.emit(this.vm.currentPage);
  }

}
