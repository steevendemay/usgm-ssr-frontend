import {Component, Input, OnInit} from '@angular/core';

export class TitleSectionComponentVm {
  titleSection: string;
}

@Component({
  selector: 'app-title-section',
  templateUrl: './title-section.component.html',
  styleUrls: ['./title-section.component.css']
})
export class TitleSectionComponent implements OnInit {

  vm: TitleSectionComponentVm = new TitleSectionComponentVm();

  @Input()
  set titleSection(value: string) {
    this.vm.titleSection = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
