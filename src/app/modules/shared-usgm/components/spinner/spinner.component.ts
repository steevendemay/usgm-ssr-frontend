import { Component, OnInit } from '@angular/core';
import {SpinnerManagerService} from '../../services/managers/spinner-manager.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  viewModel: any = {};

  constructor(private loaderService: SpinnerManagerService) {
  }

  ngOnInit() {
    this.viewModel.loading$ = this.loaderService.loading$;

  }
}
