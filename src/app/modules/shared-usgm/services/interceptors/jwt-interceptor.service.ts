import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpEventType,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';import {throwError as observableThrowError, Observable} from 'rxjs';
import {JwtManagerService} from '../managers/jwt-manager.service';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {routesConstants} from '../constants/routes';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  constructor(private jwtManager: JwtManagerService,
              private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const jwtToken = this.jwtManager.getJwt();
    let update = {};
    if (jwtToken) {
      update = {
        headers: req.headers.set('Authorization', 'Bearer ' + jwtToken)
      };
    }

    const clonedRequest = req.clone(update);

    return next
      .handle(clonedRequest)
      .pipe(
        map((event: HttpEvent<any>) => this.interceptResponse(event)),
        catchError((err: HttpErrorResponse) => this.catchJWTtokenExpire(err))
      );
  }

  interceptResponse(event: HttpEvent<any>) {
    if (event.type === HttpEventType.Response) {
      const authorization = event.headers.get('Authorization');
      if (authorization) {
        this.jwtManager.storeJwt(authorization);
      }
    }

    return event;
  }

  catchJWTtokenExpire(event: HttpErrorResponse): Observable<never> {
    try {

      if (event.status === 401) {
        // reset JWT Token
        this.jwtManager.storeJwt(false);
        this.router.navigate([routesConstants.foLogin]);
      }
    } catch (e) {
      return observableThrowError(e);
    }

    return observableThrowError(event);
  }

}
