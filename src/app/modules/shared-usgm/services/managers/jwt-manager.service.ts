import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Player} from '../../models/Player';
import {constants} from '../constants/constants';
import {User} from '../../models/User';

@Injectable({
  providedIn: 'root'
})
export class JwtManagerService {

  constructor(private http: HttpClient) { }

  storeJwt(jwt) {
    localStorage.setItem('token', jwt);
    if (jwt === null) {
      localStorage.removeItem('token');
    }
  }

  getJwt() {
    return localStorage.getItem('token');
  }

  getUserConnected() {
    return this.http.post<User>(constants.urlApi + 'details', {});
  }

  setFirstNameUserConnected(firstname) {
    localStorage.setItem('firstname', firstname);
    if (firstname === null) {
      localStorage.removeItem('firstname');
    }
  }

  getFirstNameUserConnected() {
    return localStorage.getItem('firstname');
  }
}
