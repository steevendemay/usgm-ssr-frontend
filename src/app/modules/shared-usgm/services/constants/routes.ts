export const routesConstants = {
  bo: 'admin',

  boLogin: 'connexion',
  boRegister: 'inscription',

  boAddTeam: 'ajouterEquipe',
  boUpdateTeam: 'modifierEquipe/:id',
  boUpdateTeamWithoutParam: 'modifierEquipe',
  boTeams: 'equipes',

  boAddEvent: 'ajouterEvenement',
  boUpdateEvent: 'modifierEvenement/:id',
  boUpdateEventWithoutParam: 'modifierEvenement',
  boEvents: 'evenements',

  boAddPlayer: 'ajouterJoueur',
  boUpdatePlayer: 'modifierJoueur/:id',
  boUpdatePlayerWithoutParam: 'modifierJoueur',
  boPlayers: 'joueurs',

  boMatchByTeam: 'matchsEquipe/:id',
  boMatchByTeamWithoutParam: 'matchsEquipe',
  boAddMatch: 'ajouterMatch',
  boUpdateMatch: 'modifierMatch/:id',
  boUpdateMatchWithoutParam: 'modifierMatch',

  boAddCoach: 'ajouterCoach',
  boUpdateCoach: 'modifierCoach/:id',
  boUpdateCoachWithoutParam: 'modifierCoach',
  boCoaches: 'coaches',

  boAddOffice: 'ajouterBureau',
  boUpdateOffice: 'modifierBureau/:id',
  boUpdateOfficeWithoutParam: 'modifierBureau',
  boOffices: 'bureau',

  boAddContact: 'ajouterContact',
  boUpdateContact: 'modifierContact/:id',
  boUpdateContactWithoutParam: 'modifierContact',
  boContacts: 'contacts',

  boAddSponsor: 'ajouterSponsor',
  boUpdateSponsor: 'modifierSponsor/:id',
  boUpdateSponsorWithoutParam: 'modifierSponsor',
  boSponsors: 'sponsors',

  boConvocationByTeam: 'convocationEquipe/:id',
  boConvocationByTeamWithoutParam: 'convocationEquipe',
  boAddConvocation: 'ajouterConvocation/:id',
  boAddConvocationWithoutParam: 'ajouterConvocation',
  boUpdateConvocation: 'modifierConvocation/:id',
  boUpdateConvocationWithoutParam: 'modifierConvocation',

  boActualities: 'actualites',
  boAddActuality: 'ajouterActualite',
  boUpdateActuality: 'modifierActualite/:id',
  boUpdateActualityWithoutParam: 'modifierActualite',

  boUsers: 'utilisateurs',

  boAddAlbum: 'ajouterAlbum',
  boUpdateAlbum: 'modifierAlbum/:id',
  boUpdateAlbumWithoutParam: 'modifierAlbum',
  boAlbums: 'albums',

  boPhotosAlbum: 'photosAlbum/:id',
  boPhotosAlbumWithoutParam: 'photosAlbum',

  foActualities: 'actualites',
  foAllActualities: 'toutesActualites',
  foPlayers: 'joueurs',
  foContact: 'contact',
  foConvocationsWithoutId: 'convocations',
  foConvocations: 'convocations/:id',
  foCoaches: 'dirigeants',
  foOffices: 'bureau',
  foEvents: 'evenements',
  foRegister: 'inscription',
  foLogin: 'connexion',
  foRankingScorers: 'buteurs',
  foAlbums: 'albums',
  foPhotosAlbumWithoutId: 'photosAlbum',
  foPhotosAlbum: 'photosAlbum/:id',
  foMatchesWithoutId: 'matchsEquipe',
  foMatches: 'matchsEquipe/:id',
  foActualityWithoutId: 'actualite',
  foActuality: 'actualite/:id'

};
