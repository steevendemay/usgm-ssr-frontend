import {Team} from './Team';

export class Match {
  id: number;
  homeMatch: boolean;
  teams_id: number;
  opposingTeam: string;
  dateMatch: any;
  reportDate: any;
  placeMatch: string;
  cancel: boolean;
  matchType: string;
  scoreHome: number;
  scoreAway: number;
  scoreHomeProlongation: number;
  scoreAwayProlongation: number;
  scoreHomePenalty: number;
  scoreAwayPenalty: number;
  teams: Team;

}
