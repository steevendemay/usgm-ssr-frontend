
export class Event {
  id: number;
  title: string;
  message: string;
  place: string;
  date: any;
}
