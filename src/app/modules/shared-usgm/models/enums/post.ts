export enum Post {
  GOALKEEPER = 'gardien',
  DEFENDER = 'arrière',
  MIDFIELDER = 'milieu',
  STRIKER = 'avant'
}
