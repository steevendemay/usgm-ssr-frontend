import {Team} from './Team';
import {Coach} from './Coach';
import {Match} from './Match';

export class Convocation {
  id: number;
  teams_id: number;
  players_id1: number;
  players_id2: number;
  players_id3: number;
  players_id4: number;
  players_id5: number;
  players_id6: number;
  players_id7: number;
  players_id8: number;
  players_id9: number;
  players_id10: number;
  players_id11: number;
  players_id12: number;
  players_id13: number;
  players_id14: number;
  players_id15: number;
  players_id16: number;
  players_id17: number;
  players_id18: number;
  players_id19: number;
  players_id20: number;
  coaches_id: string;
  matches_id: string;
  noMatch: boolean;
  cancel: boolean;
  convocationTime: any;
  convocationPlace: string;
  comment: string;
  teams: Team;
  coaches: Coach;
  matches: Match;

}
