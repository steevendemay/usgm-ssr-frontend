import {Actuality} from './Actuality';

export class ActualitiesPaginate {
  current_page: number;
  data: Actuality[];
  first_page_url: string;
  from: 1;
  last_page: 1;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: string;
  to: number;
  total: number
}
