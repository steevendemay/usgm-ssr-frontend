import {Post} from './enums/post';

export class Player {
  id: number;
  firstname: string;
  lastname: string;
  post: Post;
  goals: number;
  picture: string;
  birthdate: any;
}
