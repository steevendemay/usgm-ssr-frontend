
export class Actuality {
  id: number;
  title: string;
  message: string;
  picture1: any;
  picture2: any;
  picture3: any;
}
