
export class Office {
  id: number;
  firstname: string;
  lastname: string;
  phone: string;
  role: string;
  email: string;
  picture: string;

}
