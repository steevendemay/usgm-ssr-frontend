
export class User {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  token?: string;
  password?: string;
  c_password?: string;
  accept: boolean;

}
