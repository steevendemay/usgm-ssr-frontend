import {Player} from './Player';

export class Scorer {
  id: number;
  matches_id: number;
  players_id: number;
  goals: number;
  players: Player;
}
