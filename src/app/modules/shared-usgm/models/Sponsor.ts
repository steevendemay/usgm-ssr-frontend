
export class Sponsor {
  id: number;
  name: string;
  picture: string;
}
