import {Team} from './Team';

export class Coach {
  id: number;
  firstname: string;
  lastname: string;
  teams_id: number;
  phone: string;
  picture: string;
  birthdate: any;
  teams: Team;

}
