import {Convocation} from './Convocation';
import {Player} from './Player';

export class ConvocationPlayer {
  convocation: Convocation;
  players: Player[];
}
