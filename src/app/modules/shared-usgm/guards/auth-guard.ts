import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {JwtManagerService} from '../services/managers/jwt-manager.service';
import {routesConstants} from '../services/constants/routes';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private jwtManager: JwtManagerService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const canGo = !!this.jwtManager.getJwt();
    if (!canGo) {
      this.router.navigate([routesConstants.foLogin]);
    }
    return canGo;
  }
}
