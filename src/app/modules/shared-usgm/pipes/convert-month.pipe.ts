import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertMonth'
})
export class ConvertMonthPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): string {
    let toReturn = '';
    switch (value) {
      case '01': toReturn = 'Janvier';break;
      case '02': toReturn = 'Février';break;
      case '03': toReturn = 'Mars';break;
      case '04': toReturn = 'Avril';break;
      case '05': toReturn = 'Mai';break;
      case '06': toReturn = 'Juin';break;
      case '07': toReturn = 'Juillet';break;
      case '08': toReturn = 'Aout';break;
      case '09': toReturn = 'Septembre';break;
      case '10': toReturn = 'Octobre';break;
      case '11': toReturn = 'Novembre';break;
      case '12': toReturn = 'Décembre';break;
    }
    return toReturn;
  }

}
