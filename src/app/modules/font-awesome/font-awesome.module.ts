import { NgModule } from '@angular/core';
import {
  FaDuotoneIconComponent,
  FaIconComponent,
  FaLayersComponent, FaLayersCounterComponent,
  FaLayersTextComponent, FaStackComponent,
  FaStackItemSizeDirective
} from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [

  ],
  exports: [

  ],
})
export class FontAwesomeModule {}
