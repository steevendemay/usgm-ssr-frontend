import { Injectable } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class BackOfficeFormManagerService {

  constructor(private fb: FormBuilder) { }

  generateFormPlayer() {
    return this.fb.group({
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'post': ['', [Validators.required]],
      'picture': ['', []],
      'birthdate': ['', []]
    })
  }

  generateFormAlbum() {
    return this.fb.group({
      'title': ['', [Validators.required]]
    })
  }

  generateFormScorer() {
    return this.fb.group({
      'players_id': ['', [Validators.required]],
      'goals': ['', [Validators.required]],
      'matches_id': ['', [Validators.required]]
    })
  }

  generateFormCoach() {
    return this.fb.group({
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'phone': ['', [Validators.required]],
      'teams_id': ['', [Validators.required]],
      'picture': ['', []],
      'birthdate': ['', []]
    })
  }

  generateFormOffice() {
    return this.fb.group({
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'phone': ['', [Validators.required]],
      'role': ['', [Validators.required]],
      'email': ['', [Validators.required]],
      'picture': ['', []],
    })
  }

  generateFormContact() {
    return this.fb.group({
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'phone': ['', [Validators.required]],
      'email': ['', [Validators.required]],
      'picture': ['', []],
    })
  }

  generateFormSponsor() {
    return this.fb.group({
      'name': ['', [Validators.required]],
      'picture': ['', []],
    })
  }

  generateFormEvent() {
    return this.fb.group({
      'title': ['', [Validators.required]],
      'place': ['', [Validators.required]],
      'message': ['', [Validators.required]],
      'date': ['', [Validators.required]]
    })
  }

  generateFormActuality() {
    return this.fb.group({
      'title': ['', [Validators.required]],
      'message': ['', [Validators.required]],
      'picture1': ['', []],
      'picture2': ['', []],
      'picture3': ['', []]
    })
  }

  generateFormTeam() {
    return this.fb.group({
      'name': ['', [Validators.required]],
    })
  }

  generateFormMatch() {
    return this.fb.group({
      'homeMatch': [false, []],
      'teams_id': ['', [Validators.required]],
      'opposingTeam': ['', [Validators.required]],
      'dateMatch': ['', []],
      'placeMatch': ['', []],
      'cancel': [false, []],
      'matchType': ['', [Validators.required]],
      'reportDate': ['', []],
      'scoreHome': ['', []],
      'scoreAway': ['', []],
      'scoreHomeProlongation': ['', []],
      'scoreAwayProlongation': ['', []],
      'scoreHomePenalty': ['', []],
      'scoreAwayPenalty': ['', []],
    })
  }

  generateFormConvocation() {
    return this.fb.group({
      'teams_id': ['', [Validators.required]],
      'players_id1': ['', []],
      'players_id2': ['', []],
      'players_id3': ['', []],
      'players_id4': ['', []],
      'players_id5': ['', []],
      'players_id6': ['', []],
      'players_id7': ['', []],
      'players_id8': ['', []],
      'players_id9': ['', []],
      'players_id10': ['', []],
      'players_id11': ['', []],
      'players_id12': ['', []],
      'players_id13': ['', []],
      'players_id14': ['', []],
      'players_id15': ['', []],
      'players_id16': ['', []],
      'players_id17': ['', []],
      'players_id18': ['', []],
      'players_id19': ['', []],
      'players_id20': ['', []],
      'matches_id': ['', []],
      'coaches_id': ['', []],
      'noMatch': [false, []],
      'cancel': [false, []],
      'convocationTime': ['', []],
      'convocationPlace': ['', []],
      'comment': ['', []],
    })
  }
}
