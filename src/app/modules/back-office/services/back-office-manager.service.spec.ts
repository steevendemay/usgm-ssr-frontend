import { TestBed } from '@angular/core/testing';

import { BackOfficeManagerService } from './back-office-manager.service';

describe('BackOfficeManagerService', () => {
  let service: BackOfficeManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackOfficeManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
