import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {constants} from '../../shared-usgm/services/constants/constants';
import {Player} from '../../shared-usgm/models/Player';
import {Event} from '../../shared-usgm/models/Event';
import * as moment from 'moment';
import {Team} from '../../shared-usgm/models/Team';
import {Match} from '../../shared-usgm/models/Match';
import {Coach} from '../../shared-usgm/models/Coach';
import {Office} from '../../shared-usgm/models/Office';
import {Contact} from '../../shared-usgm/models/Contact';
import {Sponsor} from '../../shared-usgm/models/Sponsor';
import {Convocation} from '../../shared-usgm/models/Convocation';
import {Actuality} from '../../shared-usgm/models/Actuality';
import {User} from '../../shared-usgm/models/User';
import {ConvocationPlayer} from '../../shared-usgm/models/ConvocationPlayer';
import {Scorer} from '../../shared-usgm/models/Scorer';
import {Observable} from 'rxjs';
import {Album} from '../../shared-usgm/models/Album';
import {Photo} from '../../shared-usgm/models/Photo';
import {ActualitiesPaginate} from '../../shared-usgm/models/ActualitiesPaginate';
@Injectable({
  providedIn: 'root'
})
export class BackOfficeManagerService {

  constructor(private http: HttpClient) { }


  getPlayers() {
    return this.http.get<Player[]>(constants.urlApi + 'players');
  }

  getPlayerById(playerId: number) {
    return this.http.get<Player>(constants.urlApi + `player/${playerId}`);
  }

  deletePlayer(playerId: number) {
    return this.http.delete(constants.urlApi + `player/${playerId}`);
  }

  addPlayer(player: Player, picture) {
    const p = player;
    p.picture = picture;
    p.goals = 0;
    p.birthdate = moment(player.birthdate).format("YYYY-MM-DD HH:mm:SS");
    return this.http.post<Player>(constants.urlApi + `player/`, p );
  }

  updatePlayer(player: Player, picture, playerId: number) {
    const p = player;
    p.picture = picture;
    p.birthdate = moment(player.birthdate).format("YYYY-MM-DD HH:mm:SS");
    return this.http.put(constants.urlApi + `player/${playerId}`, p);
  }

  getEvents() {
    return this.http.get<Event[]>(constants.urlApi + 'events');
  }

  addEvent(event: Event) {
    const e = event;
    e.date= moment(event.date).format("YYYY-MM-DD HH:mm:SS");
    return this.http.post<Event>(constants.urlApi + `event/`, e );
  }

  getEventById(id: number) {
    return this.http.get<Event>(constants.urlApi + `event/${id}`);
  }

  updateEvent(event: Event, id: number) {
    const e = event;
    e.date= moment(event.date).format("YYYY-MM-DD HH:mm:SS");
    return this.http.put(constants.urlApi + `event/${id}`, e);
  }

  deleteEvent(id: number) {
    return this.http.delete(constants.urlApi + `event/${id}`);
  }

  addTeam(team: Team) {
    return this.http.post<Team>(constants.urlApi + `team/`, team );
  }

  getTeamById(id: number) {
    return this.http.get<Team>(constants.urlApi + `team/${id}`);
  }

  updateTeam(team: Team, id: number) {
    return this.http.put(constants.urlApi + `team/${id}`, team);
  }

  deleteTeam(id: number) {
    return this.http.delete(constants.urlApi + `team/${id}`);
  }

  getTeams() {
    return this.http.get<Team[]>(constants.urlApi + 'teams');
  }

  getMatchesByTeam(idTeam: number) {
    return this.http.get<Match[]>(constants.urlApi + `matchesByTeam/${idTeam}`);
  }

  addMatch(match: Match) {
    const m = match;
    m.dateMatch= moment(match.dateMatch).format("YYYY-MM-DD hh:mm:ss");
    if (match.reportDate) {
      m.reportDate= moment(match.reportDate).format("YYYY-MM-DD HH:mm:SS") || null;
    }
    return this.http.post<Match>(constants.urlApi + `match/`, m );
  }

  deleteMatch(id: number) {
    return this.http.delete(constants.urlApi + `match/${id}`);
  }

  updateMatch(match: Match, id: number) {
    const m = match;
    m.dateMatch= moment(match.dateMatch).format("YYYY-MM-DD hh:mm:ss");
    if (match.reportDate) {
      m.reportDate= moment(match.reportDate).format("YYYY-MM-DD HH:mm:SS") || null;
    }
    return this.http.put<Match>(constants.urlApi + `match/${id}`, m );
  }

  getMatchById(id: number) {
    return this.http.get<Match>(constants.urlApi + `match/${id}`);
  }

  deleteScorersAndMatches() {
    return this.http.delete(constants.urlApi + 'resetMatchAndScorer');
  }

  getCoaches() {
    return this.http.get<Coach[]>(constants.urlApi + 'coaches');
  }

  getCoachById(coachId: number) {
    return this.http.get<Coach>(constants.urlApi + `coach/${coachId}`);
  }

  deleteCoach(coachId: number) {
    return this.http.delete(constants.urlApi + `coach/${coachId}`);
  }

  addCoach(coach: Coach, picture) {
    const c = coach;
    c.picture = picture;
    coach.birthdate ? c.birthdate = moment(coach.birthdate).format("YYYY-MM-DD HH:mm:SS") : null;
    return this.http.post<Coach>(constants.urlApi + `coach/`, c );
  }

  updateCoach(coach: Coach, picture, id: number) {
    const c = coach;
    c.picture = picture;
    coach.birthdate ? c.birthdate = moment(coach.birthdate).format("YYYY-MM-DD HH:mm:SS") : null;
    return this.http.put(constants.urlApi + `coach/${id}`, c);
  }


  getOffices() {
    return this.http.get<Office[]>(constants.urlApi + 'offices');
  }

  getOfficeById(id: number) {
    return this.http.get<Office>(constants.urlApi + `office/${id}`);
  }

  deleteOffice(id: number) {
    return this.http.delete(constants.urlApi + `office/${id}`);
  }

  addOffice(office: Office, picture) {
    const o = office;
    o.picture = picture;
    return this.http.post<Office>(constants.urlApi + `office/`, o );
  }

  updateOffice(office: Office, picture, id: number) {
    const o = office;
    o.picture = picture;
    return this.http.put(constants.urlApi + `office/${id}`, o);
  }

  getContacts() {
    return this.http.get<Contact[]>(constants.urlApi + 'contacts');
  }

  getContactById(id: number) {
    return this.http.get<Contact>(constants.urlApi + `contact/${id}`);
  }

  deleteContact(id: number) {
    return this.http.delete(constants.urlApi + `contact/${id}`);
  }

  addContact(contact: Contact, picture) {
    const c = contact;
    c.picture = picture;
    return this.http.post<Contact>(constants.urlApi + `contact/`, c);
  }

  updateContact(contact: Contact, picture, id: number) {
    const c = contact;
    c.picture = picture;
    return this.http.put(constants.urlApi + `contact/${id}`, c);
  }


  getSponsors() {
    return this.http.get<Sponsor[]>(constants.urlApi + 'sponsors');
  }

  getSponsorById(id: number) {
    return this.http.get<Sponsor>(constants.urlApi + `sponsor/${id}`);
  }

  deleteSponsor(id: number) {
    return this.http.delete(constants.urlApi + `sponsor/${id}`);
  }

  addSponsor(sponsor: Sponsor, picture) {
    const s = sponsor;
    s.picture = picture;
    return this.http.post<Sponsor>(constants.urlApi + `sponsor/`, s);
  }

  updateSponsor(sponsor: Sponsor, picture, id: number) {
    const s = sponsor;
    s.picture = picture;
    return this.http.put(constants.urlApi + `sponsor/${id}`, s);
  }

  getConvocationByTeam(idTeam: number) {
    return this.http.get<ConvocationPlayer>(constants.urlApi + `convocationByTeam/${idTeam}`);
  }

  addConvocation(convocation: Convocation) {
    const c = convocation;
    if (convocation.convocationTime) {
      c.convocationTime= moment(convocation.convocationTime).format("YYYY-MM-DD HH:MM:SS") || null;
    }
    return this.http.post<Convocation>(constants.urlApi + `convocation/`, c);
  }

  updateConvocation(convocation: Convocation, id: number) {
    const c = convocation;
    if (convocation.convocationTime) {
      c.convocationTime= moment(convocation.convocationTime).format("YYYY-MM-DD HH:mm:SS") || null;
    }
    return this.http.put<Convocation>(constants.urlApi + `convocation/${id}`, c);
  }

  getConvocationById(id: number) {
    return this.http.get<ConvocationPlayer>(constants.urlApi + `convocation/${id}`);
  }

  deleteConvocation(id: number) {
    return this.http.delete(constants.urlApi + `convocation/${id}`);
  }

  getActualities(pageId: number) {
    return this.http.get<ActualitiesPaginate>(constants.urlApi + `actualities?page=${pageId}`);
  }

  getActualityById(id: number) {
    return this.http.get<Actuality>(constants.urlApi + `actuality/${id}`);
  }

  deleteActuality(id: number) {
    return this.http.delete(constants.urlApi + `actuality/${id}`);
  }

  addActuality(actuality: Actuality, picture1, picture2, picture3) {
    const a = actuality;
    a.picture1 = picture1;
    a.picture2 = picture2;
    a.picture3 = picture3;
    return this.http.post<Actuality>(constants.urlApi + `actuality/`, a);
  }

  updateActuality(actuality: Actuality, picture1, picture2, picture3,  id: number) {
    const a = actuality;
    a.picture1 = picture1;
    a.picture2 = picture2;
    a.picture3 = picture3;
    return this.http.put(constants.urlApi + `actuality/${id}`, a);
  }


  getUser() {
    return this.http.get<User>(constants.urlApi + `details`);
  }

  refuseUser(id: number) {
    return this.http.get(constants.urlApi + `refuseUser/${id}`);
  }

  acceptUser(id: number) {
    return this.http.get(constants.urlApi + `acceptUser/${id}`);
  }

  getUsers() {
    return this.http.get<User[]>(constants.urlApi + `users`);
  }

  getScorersByMatchId(id: number): Observable<Scorer[]> {
    return this.http.get<Scorer[]>(constants.urlApi + `scorersByMatch/${id}`);
  }

  addScorer(scorer: Scorer) {
    return this.http.post<Scorer>(constants.urlApi + `scorer/`, scorer);
  }

  deleteScorer(id: number) {
    return this.http.delete(constants.urlApi + `scorer/${id}`);
  }





  getAlbums() {
    return this.http.get<Album[]>(constants.urlApi + 'albums');
  }

  getAlbumById(id: number) {
    return this.http.get<Album>(constants.urlApi + `album/${id}`);
  }

  deleteAlbum(id: number) {
    return this.http.delete(constants.urlApi + `album/${id}`);
  }

  addAlbum(album: Album) {
    return this.http.post<Office>(constants.urlApi + `album/`, album );
  }

  updateAlbum(album: Album, id: number) {
    return this.http.put(constants.urlApi + `album/${id}`, album);
  }

  getPhotosByAlbumId(albumId: number) {
    return this.http.get<Photo[]>(constants.urlApi + `photosByAlbum/${albumId}`);
  }

  addPhotosAlbum(photos: Photo[]) {
    return this.http.post<Photo[]>(constants.urlApi + `photosAlbum/`, photos );
  }

  deletePhotoAlbum(id: number) {
    return this.http.delete(constants.urlApi + `photoAlbum/${id}`);
  }


}
