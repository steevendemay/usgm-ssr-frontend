import { TestBed } from '@angular/core/testing';

import { BackOfficeFormManagerService } from './back-office-form-manager.service';

describe('BackOfficeFormManagerService', () => {
  let service: BackOfficeFormManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackOfficeFormManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
