import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoMenuComponent } from './components/bo-menu/bo-menu.component';
import { BoPlayersComponent } from './views/bo-players/bo-players.component';
import {BackOfficeRoutingModule} from './back-office-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BoAddPlayerComponent } from './views/bo-add-player/bo-add-player.component';
import {SharedUsgmModule} from '../shared-usgm/shared-usgm.module';
import {NgxDatePickerModule} from '@ngx-tiny/date-picker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BoEditPlayerComponent } from './views/bo-edit-player/bo-edit-player.component';
import { BoAddEventComponent } from './views/bo-add-event/bo-add-event.component';
import { BoEditEventComponent } from './views/bo-edit-event/bo-edit-event.component';
import { BoEventsComponent } from './views/bo-events/bo-events.component';
import { BoEventFormComponent } from './components/bo-event-form/bo-event-form.component';
import { BoTeamFormComponent } from './components/bo-team-form/bo-team-form.component';
import { BoTeamsComponent } from './views/bo-teams/bo-teams.component';
import { BoEditTeamComponent } from './views/bo-edit-team/bo-edit-team.component';
import { BoAddTeamComponent } from './views/bo-add-team/bo-add-team.component';
import { BoMatchesComponent } from './views/bo-matches/bo-matches.component';
import { BoMatchFormComponent } from './components/bo-match-form/bo-match-form.component';
import { BoAddMatchComponent } from './views/bo-add-match/bo-add-match.component';
import {DlDateTimeDateModule, DlDateTimePickerModule} from 'angular-bootstrap-datetimepicker';
import { BoEditMatchComponent } from './views/bo-edit-match/bo-edit-match.component';
import { BoCoachesFormComponent } from './components/bo-coaches-form/bo-coaches-form.component';
import {BoAddCoachesComponent} from './views/bo-add-coaches/bo-add-coaches.component';
import {BoCoachesComponent} from './views/bo-coaches/bo-coaches.component';
import {BoEditCoachesComponent} from './views/bo-edit-coaches/bo-edit-coaches.component';
import { BoOfficeFormComponent } from './components/bo-office-form/bo-office-form.component';
import { BoAddOfficeComponent } from './views/bo-add-office/bo-add-office.component';
import { BoEditOfficeComponent } from './views/bo-edit-office/bo-edit-office.component';
import { BoOfficesComponent } from './views/bo-offices/bo-offices.component';
import { BoContactFormComponent } from './components/bo-contact-form/bo-contact-form.component';
import { BoAddContactComponent } from './views/bo-add-contact/bo-add-contact.component';
import { BoEditContactComponent } from './views/bo-edit-contact/bo-edit-contact.component';
import { BoContactsComponent } from './views/bo-contacts/bo-contacts.component';
import { BoSponsorFormComponent } from './components/bo-sponsor-form/bo-sponsor-form.component';
import { BoEditSponsorComponent } from './views/bo-edit-sponsor/bo-edit-sponsor.component';
import { BoAddSponsorComponent } from './views/bo-add-sponsor/bo-add-sponsor.component';
import { BoSponsorsComponent } from './views/bo-sponsors/bo-sponsors.component';
import {BoConvocationsComponent} from './views/bo-convocations/bo-convocations.component';
import {BoAddConvocationComponent} from './views/bo-add-convocation/bo-add-convocation.component';
import {BoEditConvocationComponent} from './views/bo-edit-convocation/bo-edit-convocation.component';
import { BoConvocationFormComponent } from './components/bo-convocation-form/bo-convocation-form.component';
import { BoActualityFormComponent } from './components/bo-actuality-form/bo-actuality-form.component';
import { BoActualitiesComponent } from './views/bo-actualities/bo-actualities.component';
import { BoAddActualityComponent } from './views/bo-add-actuality/bo-add-actuality.component';
import { BoEditActualityComponent } from './views/bo-edit-actuality/bo-edit-actuality.component';
import { BoUsersComponent } from './views/bo-users/bo-users.component';
import { BoScorerFormComponent } from './components/bo-scorer-form/bo-scorer-form.component';
import {JwtInterceptorService} from '../shared-usgm/services/interceptors/jwt-interceptor.service';
import { BoAlbumFormComponent } from './components/bo-album-form/bo-album-form.component';
import { BoAlbumsComponent } from './views/bo-albums/bo-albums.component';
import { BoAddAlbumComponent } from './views/bo-add-album/bo-add-album.component';
import { BoEditAlbumComponent } from './views/bo-edit-album/bo-edit-album.component';
import { BoPhotosAlbumComponent } from './views/bo-photos-album/bo-photos-album.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [BoMenuComponent, BoPlayersComponent, BoAddPlayerComponent, BoEditPlayerComponent, BoAddEventComponent, BoEditEventComponent, BoEventsComponent, BoEventFormComponent, BoTeamFormComponent, BoTeamsComponent, BoEditTeamComponent, BoAddTeamComponent, BoMatchesComponent, BoMatchFormComponent, BoAddMatchComponent, BoEditMatchComponent, BoCoachesFormComponent, BoAddCoachesComponent, BoCoachesComponent, BoEditCoachesComponent, BoOfficeFormComponent, BoAddOfficeComponent,
    BoEditOfficeComponent, BoOfficesComponent, BoContactFormComponent, BoAddContactComponent, BoEditContactComponent, BoContactsComponent, BoSponsorFormComponent, BoEditSponsorComponent, BoAddSponsorComponent, BoSponsorsComponent, BoConvocationsComponent, BoAddConvocationComponent, BoEditConvocationComponent, BoConvocationFormComponent, BoActualityFormComponent, BoActualitiesComponent, BoAddActualityComponent, BoEditActualityComponent, BoUsersComponent, BoScorerFormComponent, BoAlbumFormComponent, BoAlbumsComponent, BoAddAlbumComponent, BoEditAlbumComponent, BoPhotosAlbumComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    BackOfficeRoutingModule,
    NgxDatePickerModule,
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    DlDateTimeDateModule,
    DlDateTimePickerModule,
    SharedUsgmModule
  ],
})
export class BackOfficeModule { }
