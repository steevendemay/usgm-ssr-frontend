import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddEventComponentVM {
  fGroup: FormGroup;
}

@Component({
  selector: 'app-bo-add-event',
  templateUrl: './bo-add-event.component.html',
  styleUrls: ['./bo-add-event.component.css']
})
export class BoAddEventComponent implements OnInit {

  vm: BoAddEventComponentVM = new BoAddEventComponentVM();

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormEvent();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addEvent(this.vm.fGroup.value).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boEvents]);
      this.spinnerManager.hide();
    });
  }

}
