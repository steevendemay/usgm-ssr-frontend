import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddEventComponent } from './bo-add-event.component';

describe('BoAddEventComponent', () => {
  let component: BoAddEventComponent;
  let fixture: ComponentFixture<BoAddEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
