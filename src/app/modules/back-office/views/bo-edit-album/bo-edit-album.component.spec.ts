import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditAlbumComponent } from './bo-edit-album.component';

describe('BoEditAlbumComponent', () => {
  let component: BoEditAlbumComponent;
  let fixture: ComponentFixture<BoEditAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
