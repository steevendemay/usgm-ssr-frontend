import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {FormGroup} from '@angular/forms';
import {Album} from '../../../shared-usgm/models/Album';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditAlbumComponentVM {
  fGroup: FormGroup;
  album: Album;
}
@Component({
  selector: 'app-bo-edit-album',
  templateUrl: './bo-edit-album.component.html',
  styleUrls: ['./bo-edit-album.component.css']
})
export class BoEditAlbumComponent implements OnInit {

  vm: BoEditAlbumComponentVM = new BoEditAlbumComponentVM();
  albumId: number;

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getAlbumById();
  }

  getAlbumById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.albumId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getAlbumById(this.albumId)}
      )).subscribe((album) => {
        this.vm.fGroup = this.boFormManager.generateFormAlbum();
        this.vm.album = album;
        this.vm.fGroup.patchValue(album);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate(id: number) {
    this.spinnerManager.show();
    this.boManager.updateAlbum(this.vm.fGroup.value, this.albumId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }


}
