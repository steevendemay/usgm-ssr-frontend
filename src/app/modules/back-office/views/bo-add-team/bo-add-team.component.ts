import { Component, OnInit } from '@angular/core';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddTeamComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-team',
  templateUrl: './bo-add-team.component.html',
  styleUrls: ['./bo-add-team.component.css']
})
export class BoAddTeamComponent implements OnInit {

  vm: BoAddTeamComponentVM = new BoAddTeamComponentVM();

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private location: Location,
              private router: Router) {
  }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormTeam();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addTeam(this.vm.fGroup.value).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }
}
