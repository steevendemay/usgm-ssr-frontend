import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddTeamComponent } from './bo-add-team.component';

describe('BoAddTeamComponent', () => {
  let component: BoAddTeamComponent;
  let fixture: ComponentFixture<BoAddTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
