import { Component, OnInit } from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {Contact} from '../../../shared-usgm/models/Contact';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoContactsComponentVM {
  contacts: Contact[];
}
@Component({
  selector: 'app-bo-contacts',
  templateUrl: './bo-contacts.component.html',
  styleUrls: ['./bo-contacts.component.css']
})
export class BoContactsComponent implements OnInit {

  vm: BoContactsComponentVM = new BoContactsComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getContacts();
  }

  getContacts() {
    this.spinnerManager.show();
    this.backOfficeManager.getContacts().subscribe((contacts) => {
      this.vm.contacts = contacts;
      this.spinnerManager.hide();
    });
  }

  handleClickDeleteContact(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.deleteContact(id).subscribe(() => {
      this.getContacts();
      this.spinnerManager.hide();
    });
  }

  handleClickUpdateContact(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateContactWithoutParam, id]);
  }

  handleClickAddContact() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddContact]);
  }
}
