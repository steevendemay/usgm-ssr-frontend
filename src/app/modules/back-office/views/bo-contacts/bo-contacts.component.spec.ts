import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoContactsComponent } from './bo-contacts.component';

describe('BoContactsComponent', () => {
  let component: BoContactsComponent;
  let fixture: ComponentFixture<BoContactsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoContactsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
