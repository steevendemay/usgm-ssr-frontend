import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditCoachesComponent } from './bo-edit-coaches.component';

describe('BoEditCoachesComponent', () => {
  let component: BoEditCoachesComponent;
  let fixture: ComponentFixture<BoEditCoachesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditCoachesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditCoachesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
