import { Component, OnInit } from '@angular/core';
import {map, mergeMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {FormGroup} from '@angular/forms';
import {Coach} from '../../../shared-usgm/models/Coach';
import {$e} from 'codelyzer/angular/styles/chars';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditCoachesComponentVM {
  fGroup: FormGroup;
  coach: Coach;
}
@Component({
  selector: 'app-bo-edit-coaches',
  templateUrl: './bo-edit-coaches.component.html',
  styleUrls: ['./bo-edit-coaches.component.css']
})
export class BoEditCoachesComponent implements OnInit {

  coachId: number;
  vm: BoEditCoachesComponentVM = new BoEditCoachesComponentVM();
  picture;

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getCoachById();
  }

  getCoachById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.coachId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getCoachById(this.coachId)}
      )).subscribe((coach) => {
        this.vm.fGroup = this.boFormManager.generateFormCoach();
        this.vm.coach = coach;
        let c = coach;
        this.picture = c.picture;
        c.birthdate ? c.birthdate = new Date(c.birthdate) : c.birthdate = null;
        this.vm.fGroup.patchValue(c);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate(id: number) {
    this.spinnerManager.show();
    this.boManager.updateCoach(this.vm.fGroup.value, this.picture, this.coachId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
