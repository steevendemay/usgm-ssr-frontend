import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Post} from '../../../shared-usgm/models/enums/post';
import {ActivatedRoute} from '@angular/router';
import {map, mergeMap} from 'rxjs/operators';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {Player} from '../../../shared-usgm/models/Player';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';
import {Location} from '@angular/common';

export class BoEditPlayerComponentVM {
  fGroup: FormGroup;
  postEnum = Post;
  player: Player;
}
@Component({
  selector: 'app-bo-edit-player',
  templateUrl: './bo-edit-player.component.html',
  styleUrls: ['./bo-edit-player.component.css']
})
export class BoEditPlayerComponent implements OnInit {

  picture;
  playerId: number;
  faTimes = faTimes;
  vm: BoEditPlayerComponentVM = new BoEditPlayerComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private location: Location,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getPlayerById();
  }

  getPlayerById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.playerId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getPlayerById(this.playerId)}
        )).subscribe((player) => {
          this.vm.fGroup = this.boFormManager.generateFormPlayer();
          this.vm.player = player;
          let p = player;
          if (player.birthdate) {
            p.birthdate = new Date(player.birthdate);
          } else {
            p.birthdate = null;
          }
          this.vm.fGroup.patchValue(p);
          this.picture = p.picture;
          this.spinnerManager.hide();
      });
  }

  handleChangePicture(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture = (reader.result).toString();
    };
  }


  handleClickDeletePicture(){
    this.picture = null;
  }

  handleSubmitUpdate(){
    this.spinnerManager.show();
    this.boManager.updatePlayer(this.vm.fGroup.value, this.picture, this.playerId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
