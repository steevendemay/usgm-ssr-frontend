import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditPlayerComponent } from './bo-edit-player.component';

describe('BoEditPlayerComponent', () => {
  let component: BoEditPlayerComponent;
  let fixture: ComponentFixture<BoEditPlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditPlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
