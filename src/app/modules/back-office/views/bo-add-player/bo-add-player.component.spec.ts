import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddPlayerComponent } from './bo-add-player.component';

describe('BoAddPlayerComponent', () => {
  let component: BoAddPlayerComponent;
  let fixture: ComponentFixture<BoAddPlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddPlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
