import { Component, OnInit } from '@angular/core';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {FormGroup} from '@angular/forms';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Post} from '../../../shared-usgm/models/enums/post';
import {Location} from '@angular/common';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddPlayerComponentVM {
  fGroup: FormGroup;
  postEnum = Post;
}
@Component({
  selector: 'app-bo-add-player',
  templateUrl: './bo-add-player.component.html',
  styleUrls: ['./bo-add-player.component.css']
})
export class BoAddPlayerComponent implements OnInit {

  picture;
  vm: BoAddPlayerComponentVM = new BoAddPlayerComponentVM();

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private location: Location) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormPlayer();
  }

  handleSubmitAdd() {
    this.spinnerManager.show();
    this.boManager.addPlayer(this.vm.fGroup.value, this.picture).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

  handleChangePicture(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture = (reader.result).toString();
    };
  }

}
