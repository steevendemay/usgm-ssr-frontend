import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditConvocationComponentVM {
  fGroup: FormGroup;
  convocation: any;
}
@Component({
  selector: 'app-bo-edit-convocation',
  templateUrl: './bo-edit-convocation.component.html',
  styleUrls: ['./bo-edit-convocation.component.css']
})
export class BoEditConvocationComponent implements OnInit {


  convocationId: number;
  vm: BoEditConvocationComponentVM = new BoEditConvocationComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getConvocationById();
  }

  getConvocationById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.convocationId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getConvocationById(this.convocationId)}
      )).subscribe((convocation) => {
        this.vm.fGroup = this.boFormManager.generateFormConvocation();
        this.vm.convocation = convocation;
        console.log(convocation);
        this.vm.fGroup.patchValue(convocation.convocation);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate() {
    this.spinnerManager.show();
    this.boManager.updateConvocation(this.vm.fGroup.value, this.convocationId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
