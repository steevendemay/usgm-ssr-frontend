import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditConvocationComponent } from './bo-edit-convocation.component';

describe('BoEditConvocationComponent', () => {
  let component: BoEditConvocationComponent;
  let fixture: ComponentFixture<BoEditConvocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditConvocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditConvocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
