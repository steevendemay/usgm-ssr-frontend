import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoCoachesComponent } from './bo-coaches.component';

describe('BoCoachesComponent', () => {
  let component: BoCoachesComponent;
  let fixture: ComponentFixture<BoCoachesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoCoachesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoCoachesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
