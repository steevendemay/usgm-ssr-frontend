import { Component, OnInit } from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {Coach} from '../../../shared-usgm/models/Coach';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoCoachesComponentVM {
  coaches: Coach[];
}
@Component({
  selector: 'app-bo-coaches',
  templateUrl: './bo-coaches.component.html',
  styleUrls: ['./bo-coaches.component.css']
})
export class BoCoachesComponent implements OnInit {

  vm: BoCoachesComponentVM = new BoCoachesComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getCoaches();
  }

  getCoaches() {
    this.spinnerManager.show();
    this.backOfficeManager.getCoaches().subscribe((coaches) => {
      this.vm.coaches = coaches;
      this.spinnerManager.hide();
    });
  }

  handleClickDeleteCoach(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.deleteCoach(id).subscribe(() => {
      this.getCoaches();
      this.spinnerManager.hide();
    });
  }

  handleClickUpdateCoach(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateCoachWithoutParam, id]);
  }

  handleClickAddCoach() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddCoach]);
  }
}
