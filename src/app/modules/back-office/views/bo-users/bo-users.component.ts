import { Component, OnInit } from '@angular/core';
import {User} from '../../../shared-usgm/models/User';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoUsersComponentVM {
  users: User[];
}

@Component({
  selector: 'app-bo-users',
  templateUrl: './bo-users.component.html',
  styleUrls: ['./bo-users.component.css']
})
export class BoUsersComponent implements OnInit {

  vm: BoUsersComponentVM = new BoUsersComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.spinnerManager.show();
    this.backOfficeManager.getUsers().subscribe((users) => {
      this.vm.users = users;
      this.spinnerManager.hide();
    });
  }

  handleClickAccept(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.acceptUser(id).subscribe(() => {
      this.getUsers();
      this.spinnerManager.hide();
    });
  }

  handleClickRefuse(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.refuseUser(id).subscribe(() => {
      this.getUsers();
      this.spinnerManager.hide();
    });
  }


}
