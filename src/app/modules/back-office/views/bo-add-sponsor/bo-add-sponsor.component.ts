import { Component, OnInit } from '@angular/core';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {BoAddContactComponentVM} from '../bo-add-contact/bo-add-contact.component';
import {FormGroup} from '@angular/forms';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddSponsorComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-sponsor',
  templateUrl: './bo-add-sponsor.component.html',
  styleUrls: ['./bo-add-sponsor.component.css']
})
export class BoAddSponsorComponent implements OnInit {

  vm: BoAddSponsorComponentVM = new BoAddSponsorComponentVM();
  picture;

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormSponsor();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addSponsor(this.vm.fGroup.value, this.picture).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boSponsors]);
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
