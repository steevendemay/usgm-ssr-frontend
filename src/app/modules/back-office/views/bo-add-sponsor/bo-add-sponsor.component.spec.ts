import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddSponsorComponent } from './bo-add-sponsor.component';

describe('BoAddSponsorComponent', () => {
  let component: BoAddSponsorComponent;
  let fixture: ComponentFixture<BoAddSponsorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddSponsorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
