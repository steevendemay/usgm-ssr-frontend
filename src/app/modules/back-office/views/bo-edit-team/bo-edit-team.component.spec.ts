import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditTeamComponent } from './bo-edit-team.component';

describe('BoEditTeamComponent', () => {
  let component: BoEditTeamComponent;
  let fixture: ComponentFixture<BoEditTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
