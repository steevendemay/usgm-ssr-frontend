import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {FormGroup} from '@angular/forms';
import {Team} from '../../../shared-usgm/models/Team';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditTeamComponentVM {
  fGroup: FormGroup;
  team: Team;
}
@Component({
  selector: 'app-bo-edit-team',
  templateUrl: './bo-edit-team.component.html',
  styleUrls: ['./bo-edit-team.component.css']
})
export class BoEditTeamComponent implements OnInit {

  teamId: number;
  vm: BoEditTeamComponentVM = new BoEditTeamComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getTeamById();
  }

  getTeamById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.teamId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getTeamById(this.teamId)}
      )).subscribe((team) => {
        this.vm.fGroup = this.boFormManager.generateFormTeam();
        this.vm.team = team;
        this.vm.fGroup.patchValue(team);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate(id: number) {
    this.spinnerManager.show();
    this.boManager.updateTeam(this.vm.fGroup.value, this.teamId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
