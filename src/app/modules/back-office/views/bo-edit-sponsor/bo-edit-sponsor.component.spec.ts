import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditSponsorComponent } from './bo-edit-sponsor.component';

describe('BoEditSponsorComponent', () => {
  let component: BoEditSponsorComponent;
  let fixture: ComponentFixture<BoEditSponsorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditSponsorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
