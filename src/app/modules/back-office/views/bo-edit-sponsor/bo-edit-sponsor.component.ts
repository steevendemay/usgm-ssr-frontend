import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {FormGroup} from '@angular/forms';
import {Sponsor} from '../../../shared-usgm/models/Sponsor';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditSponsorComponentVM {
  fGroup: FormGroup;
  sponsor: Sponsor;
}
@Component({
  selector: 'app-bo-edit-sponsor',
  templateUrl: './bo-edit-sponsor.component.html',
  styleUrls: ['./bo-edit-sponsor.component.css']
})
export class BoEditSponsorComponent implements OnInit {

  id: number;
  vm: BoEditSponsorComponentVM = new BoEditSponsorComponentVM();
  picture;

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getSponsorById();
  }

  getSponsorById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.id = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getSponsorById(this.id)}
      )).subscribe((sponsor) => {
        this.vm.fGroup = this.boFormManager.generateFormSponsor();
        this.vm.sponsor = sponsor;
        let s = sponsor;
        this.picture = s.picture;
        this.vm.fGroup.patchValue(s);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate() {
    this.spinnerManager.show();
    this.boManager.updateSponsor(this.vm.fGroup.value, this.picture, this.id).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
