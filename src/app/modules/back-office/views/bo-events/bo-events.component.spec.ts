import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEventsComponent } from './bo-events.component';

describe('BoEventsComponent', () => {
  let component: BoEventsComponent;
  let fixture: ComponentFixture<BoEventsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEventsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
