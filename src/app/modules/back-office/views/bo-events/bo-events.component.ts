import { Component, OnInit } from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {Event} from '../../../shared-usgm/models/Event';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {Spinner} from '@angular-devkit/build-angular/src/utils/spinner';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEventsComponentVM {
  events: Event[];
}
@Component({
  selector: 'app-bo-events',
  templateUrl: './bo-events.component.html',
  styleUrls: ['./bo-events.component.css']
})
export class BoEventsComponent implements OnInit {

  vm: BoEventsComponentVM = new BoEventsComponentVM();
  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getEvents();
  }

  getEvents() {
    this.spinnerManager.show();
    this.backOfficeManager.getEvents().subscribe((events) => {
      this.vm.events = events;
      this.spinnerManager.hide();
    });
  }

  handleClickAddEvent() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddEvent]);
  }

  handleClickUpdateEvent(id: number){
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateEventWithoutParam, id]);
  }

  handleClickDeleteEvent(id: number){
    this.spinnerManager.show();
    this.backOfficeManager.deleteEvent(id).subscribe(() => {
      this.getEvents();
      this.spinnerManager.hide();
    });
  }
}
