import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoConvocationsComponent } from './bo-convocations.component';

describe('BoConvocationsComponent', () => {
  let component: BoConvocationsComponent;
  let fixture: ComponentFixture<BoConvocationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoConvocationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoConvocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
