import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {Convocation} from '../../../shared-usgm/models/Convocation';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoConvocationsComponentVM {
  convocation: Convocation;
}
@Component({
  selector: 'app-bo-convocations',
  templateUrl: './bo-convocations.component.html',
  styleUrls: ['./bo-convocations.component.css']
})
export class BoConvocationsComponent implements OnInit {

  vm: BoConvocationsComponentVM = new BoConvocationsComponentVM();
  teamId: number;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getConvocationByTeamId();
  }

  getConvocationByTeamId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.teamId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getConvocationByTeam(this.teamId)}
      )).subscribe((convocation) => {
        this.vm.convocation = convocation.convocation;
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate(id: number){
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateConvocationWithoutParam, id]);
  }

  handleClickDelete(id: number){
    this.spinnerManager.show();
    this.boManager.deleteConvocation(id).subscribe(() => {
      this.getConvocationByTeamId();
      this.spinnerManager.hide();
    });
  }

  handleClickAdd() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddConvocationWithoutParam, this.teamId])
  }

}
