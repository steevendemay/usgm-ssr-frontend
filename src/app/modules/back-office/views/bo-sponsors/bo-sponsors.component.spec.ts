import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoSponsorsComponent } from './bo-sponsors.component';

describe('BoSponsorsComponent', () => {
  let component: BoSponsorsComponent;
  let fixture: ComponentFixture<BoSponsorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoSponsorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoSponsorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
