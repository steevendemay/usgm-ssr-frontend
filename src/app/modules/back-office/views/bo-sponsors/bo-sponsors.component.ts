import { Component, OnInit } from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {Sponsor} from '../../../shared-usgm/models/Sponsor';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoSponsorsComponentVM {
  sponsors: Sponsor[];
}
@Component({
  selector: 'app-bo-sponsors',
  templateUrl: './bo-sponsors.component.html',
  styleUrls: ['./bo-sponsors.component.css']
})
export class BoSponsorsComponent implements OnInit {

  vm: BoSponsorsComponentVM = new BoSponsorsComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getSponsors();
  }

  getSponsors() {
    this.spinnerManager.show();
    this.backOfficeManager.getSponsors().subscribe((sponsors) => {
      this.vm.sponsors = sponsors;
      this.spinnerManager.hide();
    });
  }

  handleClickDeleteSponsor(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.deleteSponsor(id).subscribe(() => {
      this.getSponsors();
      this.spinnerManager.hide();
    });
  }

  handleClickUpdateSponsor(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateSponsorWithoutParam, id]);
  }

  handleClickAddSponsor() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddSponsor]);
  }
}
