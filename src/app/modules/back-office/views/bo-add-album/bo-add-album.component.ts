import { Component, OnInit } from '@angular/core';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {FormGroup} from '@angular/forms';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddAlbumComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-album',
  templateUrl: './bo-add-album.component.html',
  styleUrls: ['./bo-add-album.component.css']
})
export class BoAddAlbumComponent implements OnInit {


  vm: BoAddAlbumComponentVM = new BoAddAlbumComponentVM();

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormAlbum();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addAlbum(this.vm.fGroup.value).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boAlbums]);
      this.spinnerManager.hide();
    });
  }


}
