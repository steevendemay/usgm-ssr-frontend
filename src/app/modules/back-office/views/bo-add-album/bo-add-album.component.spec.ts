import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddAlbumComponent } from './bo-add-album.component';

describe('BoAddAlbumComponent', () => {
  let component: BoAddAlbumComponent;
  let fixture: ComponentFixture<BoAddAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
