import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditContactComponent } from './bo-edit-contact.component';

describe('BoEditContactComponent', () => {
  let component: BoEditContactComponent;
  let fixture: ComponentFixture<BoEditContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
