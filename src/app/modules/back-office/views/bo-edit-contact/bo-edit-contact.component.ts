import { Component, OnInit } from '@angular/core';
import {map, mergeMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {FormGroup} from '@angular/forms';
import {Contact} from '../../../shared-usgm/models/Contact';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditContactComponentVM {
  fGroup: FormGroup;
  contact: Contact;
}
@Component({
  selector: 'app-bo-edit-contact',
  templateUrl: './bo-edit-contact.component.html',
  styleUrls: ['./bo-edit-contact.component.css']
})
export class BoEditContactComponent implements OnInit {

  id: number;
  vm: BoEditContactComponentVM = new BoEditContactComponentVM();
  picture;

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getContactById();
  }

  getContactById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.id = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getContactById(this.id)}
      )).subscribe((contact) => {
        this.vm.fGroup = this.boFormManager.generateFormContact();
        this.vm.contact = contact;
        let c = contact;
        this.picture = c.picture;
        this.vm.fGroup.patchValue(c);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate() {
    this.spinnerManager.show();
    this.boManager.updateContact(this.vm.fGroup.value, this.picture, this.id).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
