import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoActualitiesComponent } from './bo-actualities.component';

describe('BoActualitiesComponent', () => {
  let component: BoActualitiesComponent;
  let fixture: ComponentFixture<BoActualitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoActualitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoActualitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
