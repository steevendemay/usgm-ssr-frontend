import { Component, OnInit } from '@angular/core';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {Actuality} from '../../../shared-usgm/models/Actuality';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoActualitiesComponentVM {
  actualities: Actuality[];
  pageId: number = 1;
  nbPages: number;
}
@Component({
  selector: 'app-bo-actualities',
  templateUrl: './bo-actualities.component.html',
  styleUrls: ['./bo-actualities.component.css']
})
export class BoActualitiesComponent implements OnInit {

  vm: BoActualitiesComponentVM = new BoActualitiesComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getActualities();
  }

  getActualities() {
    this.spinnerManager.show();
    this.backOfficeManager.getActualities(this.vm.pageId).subscribe((actualitiesPaginate) => {
      this.vm.actualities = actualitiesPaginate.data;
      this.vm.nbPages = actualitiesPaginate.last_page;
      this.spinnerManager.hide();
    });
  }

  handleClickDeleteActuality(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.deleteActuality(id).subscribe(() => {
      this.getActualities();
      this.spinnerManager.hide();
    });
  }

  handleClickUpdateActuality(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateActualityWithoutParam, id]);
  }

  handleClickAddActuality() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddActuality]);
  }

  handleChangePage(event: number) {
    this.vm.pageId = event;
    this.getActualities();
  }

}
