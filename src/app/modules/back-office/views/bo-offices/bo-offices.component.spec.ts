import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoOfficesComponent } from './bo-offices.component';

describe('BoOfficesComponent', () => {
  let component: BoOfficesComponent;
  let fixture: ComponentFixture<BoOfficesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoOfficesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoOfficesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
