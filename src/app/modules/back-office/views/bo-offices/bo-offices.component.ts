import { Component, OnInit } from '@angular/core';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {Office} from '../../../shared-usgm/models/Office';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoOfficesComponentVM {
  offices: Office[];
}

@Component({
  selector: 'app-bo-offices',
  templateUrl: './bo-offices.component.html',
  styleUrls: ['./bo-offices.component.css']
})
export class BoOfficesComponent implements OnInit {


  vm: BoOfficesComponentVM = new BoOfficesComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getOffices();
  }

  getOffices() {
    this.spinnerManager.show();
    this.backOfficeManager.getOffices().subscribe((offices) => {
      this.vm.offices = offices;
      this.spinnerManager.hide();
    });
  }

  handleClickDeleteOffice(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.deleteOffice(id).subscribe(() => {
      this.getOffices();
      this.spinnerManager.hide();
    });
  }

  handleClickUpdateOffice(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateOfficeWithoutParam, id]);
  }

  handleClickAddOffice() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddOffice]);
  }
}
