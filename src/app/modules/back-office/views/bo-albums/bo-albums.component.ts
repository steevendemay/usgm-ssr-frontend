import { Component, OnInit } from '@angular/core';
import {Album} from '../../../shared-usgm/models/Album';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAlbumsComponentVM {
  albums: Album[];
}
@Component({
  selector: 'app-bo-albums',
  templateUrl: './bo-albums.component.html',
  styleUrls: ['./bo-albums.component.css']
})
export class BoAlbumsComponent implements OnInit {

  vm: BoAlbumsComponentVM = new BoAlbumsComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getAlbums();
  }


  getAlbums() {
    this.spinnerManager.show();
    this.backOfficeManager.getAlbums().subscribe((albums) => {
      this.vm.albums = albums;
      this.spinnerManager.hide();
    });
  }

  handleClickDeleteAlbum(id: number) {
    this.spinnerManager.show();
    this.backOfficeManager.deleteAlbum(id).subscribe(() => {
      this.getAlbums();
      this.spinnerManager.hide();
    });
  }

  handleClickUpdateAlbum(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateAlbumWithoutParam, id]);
  }

  handleClickAddAlbum() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddAlbum]);
  }

  handleClickAddPhotos(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boPhotosAlbumWithoutParam, id]);
  }

}
