import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAlbumsComponent } from './bo-albums.component';

describe('BoAlbumsComponent', () => {
  let component: BoAlbumsComponent;
  let fixture: ComponentFixture<BoAlbumsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAlbumsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
