import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {FormGroup} from '@angular/forms';
import {Office} from '../../../shared-usgm/models/Office';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditOfficeComponentVM {
  fGroup: FormGroup;
  office: Office;
}
@Component({
  selector: 'app-bo-edit-office',
  templateUrl: './bo-edit-office.component.html',
  styleUrls: ['./bo-edit-office.component.css']
})
export class BoEditOfficeComponent implements OnInit {

  id: number;
  vm: BoEditOfficeComponentVM = new BoEditOfficeComponentVM();
  picture;

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getOfficeById();
  }

  getOfficeById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.id = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getOfficeById(this.id)}
      )).subscribe((office) => {
        this.vm.fGroup = this.boFormManager.generateFormOffice();
        this.vm.office = office;
        let o = office;
        this.picture = o.picture;
        this.vm.fGroup.patchValue(o);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate() {
    this.spinnerManager.show();
    this.boManager.updateOffice(this.vm.fGroup.value, this.picture, this.id).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
