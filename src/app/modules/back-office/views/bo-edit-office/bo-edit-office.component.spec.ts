import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditOfficeComponent } from './bo-edit-office.component';

describe('BoEditOfficeComponent', () => {
  let component: BoEditOfficeComponent;
  let fixture: ComponentFixture<BoEditOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditOfficeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
