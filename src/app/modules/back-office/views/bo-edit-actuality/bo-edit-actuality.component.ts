import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {Actuality} from '../../../shared-usgm/models/Actuality';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditActualityComponentVM {
  fGroup: FormGroup;
  actuality: Actuality;
}
@Component({
  selector: 'app-bo-edit-actuality',
  templateUrl: './bo-edit-actuality.component.html',
  styleUrls: ['./bo-edit-actuality.component.css']
})
export class BoEditActualityComponent implements OnInit {

  vm: BoEditActualityComponentVM = new BoEditActualityComponentVM();
  picture1;
  picture2;
  picture3;
  id: number;

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getActualityById();
  }

  getActualityById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.id = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getActualityById(this.id)}
      )).subscribe((actuality) => {
        this.vm.fGroup = this.boFormManager.generateFormActuality();
        this.vm.actuality = actuality;
        let a = actuality;
        this.picture1 = a.picture1;
        this.picture2 = a.picture2;
        this.picture3 = a.picture3;
        this.vm.fGroup.patchValue(a);
        this.spinnerManager.hide();
      });
  }


  handleClickUpdate() {
    this.spinnerManager.show();
    this.boManager.updateActuality(this.vm.fGroup.value, this.picture1, this.picture2, this.picture3, this.id).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

  handlePic1Event($event) {
    this.picture1 = $event;
  }

  handlePic2Event($event) {
    this.picture2 = $event;
  }

  handlePic3Event($event) {
    this.picture3 = $event;
  }


}
