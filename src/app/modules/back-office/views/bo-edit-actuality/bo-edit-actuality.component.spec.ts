import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditActualityComponent } from './bo-edit-actuality.component';

describe('BoEditActualityComponent', () => {
  let component: BoEditActualityComponent;
  let fixture: ComponentFixture<BoEditActualityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditActualityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditActualityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
