import { Component, OnInit } from '@angular/core';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {BoAddOfficeComponentVM} from '../bo-add-office/bo-add-office.component';
import {FormGroup} from '@angular/forms';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddContactComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-contact',
  templateUrl: './bo-add-contact.component.html',
  styleUrls: ['./bo-add-contact.component.css']
})
export class BoAddContactComponent implements OnInit {

  vm: BoAddContactComponentVM = new BoAddContactComponentVM();
  picture;

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormContact();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addContact(this.vm.fGroup.value, this.picture).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boContacts]);
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
