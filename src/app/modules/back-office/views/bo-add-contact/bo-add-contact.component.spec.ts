import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddContactComponent } from './bo-add-contact.component';

describe('BoAddContactComponent', () => {
  let component: BoAddContactComponent;
  let fixture: ComponentFixture<BoAddContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
