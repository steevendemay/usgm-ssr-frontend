import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {Match} from '../../../shared-usgm/models/Match';
import {faInfo} from '@fortawesome/free-solid-svg-icons';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoMatchesComponentVM {
  matches: Match[];
}
@Component({
  selector: 'app-bo-matches',
  templateUrl: './bo-matches.component.html',
  styleUrls: ['./bo-matches.component.css']
})
export class BoMatchesComponent implements OnInit {

  teamId: number;
  faInfo = faInfo;
  vm: BoMatchesComponentVM  = new BoMatchesComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getMatchesByTeamId();
  }

  getMatchesByTeamId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.teamId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getMatchesByTeam(this.teamId)}
      )).subscribe((matches) => {
        this.vm.matches = matches;
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate(id: number){
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateMatchWithoutParam, id]);
  }

  handleClickDelete(id: number){
    this.spinnerManager.show();
    this.boManager.deleteMatch(id).subscribe(() => {
      this.getMatchesByTeamId();
      this.spinnerManager.hide();
    });
  }

  handleClickAdd() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddMatch]);
  }

  handleClickDeleteAll() {
    this.spinnerManager.show();
    this.boManager.deleteScorersAndMatches().subscribe(() => {
      this.getMatchesByTeamId();
      this.spinnerManager.hide();
    });
  }

}
