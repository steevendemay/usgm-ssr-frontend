import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoMatchesComponent } from './bo-matches.component';

describe('BoMatchesComponent', () => {
  let component: BoMatchesComponent;
  let fixture: ComponentFixture<BoMatchesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoMatchesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoMatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
