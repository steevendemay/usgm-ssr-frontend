import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddConvocationComponent } from './bo-add-convocation.component';

describe('BoAddConvocationComponent', () => {
  let component: BoAddConvocationComponent;
  let fixture: ComponentFixture<BoAddConvocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddConvocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddConvocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
