import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {map, mergeMap} from 'rxjs/operators';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddConvocationComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-convocation',
  templateUrl: './bo-add-convocation.component.html',
  styleUrls: ['./bo-add-convocation.component.css']
})
export class BoAddConvocationComponent implements OnInit {

  vm: BoAddConvocationComponentVM = new BoAddConvocationComponentVM();

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.getTeamId();
  }

  getTeamId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.generateForm();
        this.vm.fGroup.get('teams_id').patchValue(parseInt(params.id));
      })).subscribe(() => {
        this.spinnerManager.hide();
      });
  }

  generateForm(){
    this.vm.fGroup = this.boFormManager.generateFormConvocation();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addConvocation(this.vm.fGroup.value).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
