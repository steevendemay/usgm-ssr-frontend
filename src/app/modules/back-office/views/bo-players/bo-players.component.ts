import { Component, OnInit } from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Player} from '../../../shared-usgm/models/Player';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';


export class BoPlayersComponentVM {
  players: Player[];
}
@Component({
  selector: 'app-bo-players',
  templateUrl: './bo-players.component.html',
  styleUrls: ['./bo-players.component.css']
})
export class BoPlayersComponent implements OnInit {

  vm: BoPlayersComponentVM = new BoPlayersComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getPlayers();
  }

  getPlayers() {
    this.backOfficeManager.getPlayers().subscribe((players) => this.vm.players = players);
  }

  handleClickDeletePlayer(id: number) {
    this.backOfficeManager.deletePlayer(id).subscribe(() => {
      this.getPlayers();
    });
  }

  handleClickUpdatePlayer(id: number) {
    this.router.navigate([routesConstants.bo, routesConstants.boUpdatePlayerWithoutParam, id]);
  }

  handleClickAddPlayer() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddPlayer]);
  }

}
