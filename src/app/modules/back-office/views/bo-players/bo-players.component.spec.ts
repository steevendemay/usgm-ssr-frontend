import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoPlayersComponent } from './bo-players.component';

describe('BoPlayersComponent', () => {
  let component: BoPlayersComponent;
  let fixture: ComponentFixture<BoPlayersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoPlayersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoPlayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
