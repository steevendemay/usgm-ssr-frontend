import { Component, OnInit } from '@angular/core';
import {map, mergeMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {FormGroup} from '@angular/forms';
import {Event} from '../../../shared-usgm/models/Event';
import {Location} from '@angular/common';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditEventComponentVM {
  fGroup: FormGroup;
  event: Event;
}
@Component({
  selector: 'app-bo-edit-event',
  templateUrl: './bo-edit-event.component.html',
  styleUrls: ['./bo-edit-event.component.css']
})
export class BoEditEventComponent implements OnInit {

  eventId: number;
  vm: BoEditEventComponentVM = new BoEditEventComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getEventById();
  }

  getEventById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.eventId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getEventById(this.eventId)}
      )).subscribe((event) => {
        this.vm.fGroup = this.boFormManager.generateFormEvent();
        this.vm.event = event;
        let e = event;
        e.date = new Date(event.date);
        this.vm.fGroup.patchValue(e);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate(id: number) {
    this.spinnerManager.show();
    this.boManager.updateEvent(this.vm.fGroup.value, this.eventId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
