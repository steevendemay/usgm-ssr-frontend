import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditEventComponent } from './bo-edit-event.component';

describe('BoEditEventComponent', () => {
  let component: BoEditEventComponent;
  let fixture: ComponentFixture<BoEditEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
