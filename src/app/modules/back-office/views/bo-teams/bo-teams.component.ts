import { Component, OnInit } from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {Team} from '../../../shared-usgm/models/Team';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoTeamsComponentVM {
  teams: Team[];
}
@Component({
  selector: 'app-bo-teams',
  templateUrl: './bo-teams.component.html',
  styleUrls: ['./bo-teams.component.css']
})
export class BoTeamsComponent implements OnInit {

  vm: BoTeamsComponentVM = new BoTeamsComponentVM();

  constructor(private backOfficeManager: BackOfficeManagerService,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getTeams();
  }

  getTeams() {
    this.spinnerManager.show();
    this.backOfficeManager.getTeams().subscribe((teams) => {
      this.vm.teams = teams;
      this.spinnerManager.hide();
    });
  }

  handleClickAddTeam() {
    this.router.navigate([routesConstants.bo, routesConstants.boAddTeam])
  }

  handleClickUpdateTeam(id: number){
    this.router.navigate([routesConstants.bo, routesConstants.boUpdateTeamWithoutParam, id])
  }

  handleClickDeleteTeam(id: number){
    this.spinnerManager.show();
    this.backOfficeManager.deleteTeam(id).subscribe(() => {
      this.getTeams();
      this.spinnerManager.hide();
    });
  }
}
