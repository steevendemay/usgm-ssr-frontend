import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoTeamsComponent } from './bo-teams.component';

describe('BoTeamsComponent', () => {
  let component: BoTeamsComponent;
  let fixture: ComponentFixture<BoTeamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoTeamsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
