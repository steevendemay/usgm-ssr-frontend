import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddActualityComponent } from './bo-add-actuality.component';

describe('BoAddActualityComponent', () => {
  let component: BoAddActualityComponent;
  let fixture: ComponentFixture<BoAddActualityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddActualityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddActualityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
