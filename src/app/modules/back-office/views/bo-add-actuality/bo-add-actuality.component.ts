import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddActualityComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-actuality',
  templateUrl: './bo-add-actuality.component.html',
  styleUrls: ['./bo-add-actuality.component.css']
})
export class BoAddActualityComponent implements OnInit {

  vm: BoAddActualityComponentVM = new BoAddActualityComponentVM();
  picture1;
  picture2;
  picture3;

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormActuality();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addActuality(this.vm.fGroup.value, this.picture1, this.picture2, this.picture3).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boActualities]);
      this.spinnerManager.hide();
    });
  }

  handlePic1Event($event) {
    this.picture1 = $event;
  }

  handlePic2Event($event) {
    this.picture2 = $event;
  }

  handlePic3Event($event) {
    this.picture3 = $event;
  }


}
