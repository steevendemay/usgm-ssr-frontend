import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEditMatchComponent } from './bo-edit-match.component';

describe('BoEditMatchComponent', () => {
  let component: BoEditMatchComponent;
  let fixture: ComponentFixture<BoEditMatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEditMatchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEditMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
