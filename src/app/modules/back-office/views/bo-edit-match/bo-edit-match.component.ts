import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {FormGroup} from '@angular/forms';
import {Match} from '../../../shared-usgm/models/Match';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoEditMatchComponentVM{
  fGroup: FormGroup;
  match: Match;
}
@Component({
  selector: 'app-bo-edit-match',
  templateUrl: './bo-edit-match.component.html',
  styleUrls: ['./bo-edit-match.component.css']
})
export class BoEditMatchComponent implements OnInit {


  matchId: number;
  vm: BoEditMatchComponentVM = new BoEditMatchComponentVM();

  constructor(private activatedRoute: ActivatedRoute,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.getMatchById();
  }

  getMatchById() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.matchId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getMatchById(this.matchId)}
      )).subscribe((match) => {
        this.vm.fGroup = this.boFormManager.generateFormMatch();
        this.vm.match = match;
        this.vm.fGroup.patchValue(match);
        this.spinnerManager.hide();
      });
  }

  handleClickUpdate() {
    this.spinnerManager.show();
    this.boManager.updateMatch(this.vm.fGroup.value, this.matchId).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
