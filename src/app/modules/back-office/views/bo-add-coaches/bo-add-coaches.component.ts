import { Component, OnInit } from '@angular/core';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {FormGroup} from '@angular/forms';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddCoachesComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-coaches',
  templateUrl: './bo-add-coaches.component.html',
  styleUrls: ['./bo-add-coaches.component.css']
})
export class BoAddCoachesComponent implements OnInit {
  vm: BoAddCoachesComponentVM = new BoAddCoachesComponentVM();

  picture;

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormCoach();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addCoach(this.vm.fGroup.value, this.picture).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boCoaches]);
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
