import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddCoachesComponent } from './bo-add-coaches.component';

describe('BoAddCoachesComponent', () => {
  let component: BoAddCoachesComponent;
  let fixture: ComponentFixture<BoAddCoachesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddCoachesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddCoachesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
