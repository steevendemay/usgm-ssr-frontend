import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddOfficeComponent } from './bo-add-office.component';

describe('BoAddOfficeComponent', () => {
  let component: BoAddOfficeComponent;
  let fixture: ComponentFixture<BoAddOfficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddOfficeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
