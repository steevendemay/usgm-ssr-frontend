import { Component, OnInit } from '@angular/core';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';
import {FormGroup} from '@angular/forms';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddOfficeComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-office',
  templateUrl: './bo-add-office.component.html',
  styleUrls: ['./bo-add-office.component.css']
})
export class BoAddOfficeComponent implements OnInit {

  vm: BoAddOfficeComponentVM = new BoAddOfficeComponentVM();
  picture;

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormOffice();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addOffice(this.vm.fGroup.value, this.picture).subscribe(() => {
      this.router.navigate([routesConstants.bo, routesConstants.boOffices]);
      this.spinnerManager.hide();
    });
  }

  handlePicEvent($event) {
    this.picture = $event;
  }

}
