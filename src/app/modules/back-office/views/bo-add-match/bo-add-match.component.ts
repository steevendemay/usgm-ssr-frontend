import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoAddMatchComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-add-match',
  templateUrl: './bo-add-match.component.html',
  styleUrls: ['./bo-add-match.component.css']
})
export class BoAddMatchComponent implements OnInit {

  vm: BoAddMatchComponentVM = new BoAddMatchComponentVM();

  constructor(private boFormManager: BackOfficeFormManagerService,
              private boManager: BackOfficeManagerService,
              private location: Location,
              private spinnerManager: SpinnerManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.generateForm();
  }

  generateForm(){
    this.vm.fGroup = this.boFormManager.generateFormMatch();
  }

  handleClickAdd() {
    this.spinnerManager.show();
    this.boManager.addMatch(this.vm.fGroup.value).subscribe(() => {
      this.location.back();
      this.spinnerManager.hide();
    });
  }

}
