import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAddMatchComponent } from './bo-add-match.component';

describe('BoAddMatchComponent', () => {
  let component: BoAddMatchComponent;
  let fixture: ComponentFixture<BoAddMatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAddMatchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAddMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
