import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoPhotosAlbumComponent } from './bo-photos-album.component';

describe('BoPhotosAlbumComponent', () => {
  let component: BoPhotosAlbumComponent;
  let fixture: ComponentFixture<BoPhotosAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoPhotosAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoPhotosAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
