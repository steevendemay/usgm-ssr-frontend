import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {map, mergeMap} from 'rxjs/operators';
import {Photo} from '../../../shared-usgm/models/Photo';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {SpinnerManagerService} from '../../../shared-usgm/services/managers/spinner-manager.service';

export class BoPhotosAlbumComponentVM {
  photos: Photo[];
  faTimes = faTimes;
  pict;
  showModal: boolean;
  picturesLoad = [];
}

@Component({
  selector: 'app-bo-photos-album',
  templateUrl: './bo-photos-album.component.html',
  styleUrls: ['./bo-photos-album.component.css']
})
export class BoPhotosAlbumComponent implements OnInit {

  albumId: number;
  vm: BoPhotosAlbumComponentVM = new BoPhotosAlbumComponentVM();


  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private spinnerManager: SpinnerManagerService,
              private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getPhotosByAlbumId();
  }

  handleClickShow(pic) {
    this.vm.pict = pic;
    this.vm.showModal = true;
  }

  handleClickHide() {
    this.vm.showModal = false;
  }

  handleClickSendPhotos() {
    this.spinnerManager.show();
    const photosSend: Photo[] = [];
    for (let i = 0; i < this.vm.picturesLoad.length; i++) {
      const photo: Photo = {
        picture: this.vm.picturesLoad[i],
        albums_id: this.albumId
      };
      photosSend.push(photo);
    }
    this.boManager.addPhotosAlbum(photosSend).subscribe(() => {
      this.getPhotosByAlbumId();
      this.vm.picturesLoad = [];
      this.spinnerManager.hide();
    });
  }

  getPhotosByAlbumId() {
    this.spinnerManager.show();
    return this.activatedRoute
      .params
      .pipe(map(params => {
        this.albumId = params.id;
      })).pipe(mergeMap(params => {
        return this.boManager.getPhotosByAlbumId(this.albumId)}
      )).subscribe((photos) => {
        this.vm.photos = photos;
        this.spinnerManager.hide();
      });
  }

  handleChangePictures(event) {
    let files = event.target.files;

    let file;
    for (let i=0; i<files.length ; i++){
      let reader = new FileReader();
      file = files [i];
      reader.onload = (file) => {
        this.vm.picturesLoad.push(reader.result);
      };
      reader.readAsDataURL(file)
    }
  }

  handleClickDeletePicture(id: number) {
    this.boManager.deletePhotoAlbum(id).subscribe(() => this.getPhotosByAlbumId());
  }

  handleClickDeletePictureLoad(pic) {
    let index = this.vm.picturesLoad.indexOf(pic);
    if (index !== -1) {
      this.vm.picturesLoad.splice(index, 1);
    }
  }

}
