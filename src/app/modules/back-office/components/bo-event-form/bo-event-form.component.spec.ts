import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoEventFormComponent } from './bo-event-form.component';

describe('BoEventFormComponent', () => {
  let component: BoEventFormComponent;
  let fixture: ComponentFixture<BoEventFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoEventFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoEventFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
