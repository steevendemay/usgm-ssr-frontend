import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

export class BoEventFormComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-event-form',
  templateUrl: './bo-event-form.component.html',
  styleUrls: ['./bo-event-form.component.css']
})
export class BoEventFormComponent implements OnInit {

  vm: BoEventFormComponentVM = new BoEventFormComponentVM();

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

  handleChangeDate(value) {
    this.vm.fGroup.get('date').setValue(value._value);
  }

}
