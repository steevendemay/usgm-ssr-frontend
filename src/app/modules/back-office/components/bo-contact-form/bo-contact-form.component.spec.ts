import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoContactFormComponent } from './bo-contact-form.component';

describe('BoContactFormComponent', () => {
  let component: BoContactFormComponent;
  let fixture: ComponentFixture<BoContactFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoContactFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
