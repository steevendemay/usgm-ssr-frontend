import {Component, Input, OnInit} from '@angular/core';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Team} from '../../../shared-usgm/models/Team';
import {Player} from '../../../shared-usgm/models/Player';
import {FormGroup} from '@angular/forms';
import {Coach} from '../../../shared-usgm/models/Coach';
import {Match} from '../../../shared-usgm/models/Match';

export class BoConvocationFormComponentVM {
  teams: Team[];
  players: Player[];
  coaches: Coach[];
  matches: Match[];
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-convocation-form',
  templateUrl: './bo-convocation-form.component.html',
  styleUrls: ['./bo-convocation-form.component.css']
})
export class BoConvocationFormComponent implements OnInit {

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
  }

  @Input() teamId: number;

  vm: BoConvocationFormComponentVM = new BoConvocationFormComponentVM();

  constructor(private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getTeams();
    this.getPlayers();
    this.getCoaches();
    this.getMatches();
  }

  getTeams() {
    this.boManager.getTeams().subscribe((teams) => {
      this.vm.teams = teams;
    });
  }

  getPlayers() {
    this.boManager.getPlayers().subscribe((players) => {
      this.vm.players = players;
    });
  }

  getCoaches() {
    this.boManager.getCoaches().subscribe((coaches) => {
      this.vm.coaches = coaches;
    });
  }

  getMatches() {
    this.boManager.getMatchesByTeam(this.vm.fGroup.get('teams_id').value).subscribe((matches) => {
      this.vm.matches = matches;
    });
  }

  handleChangeDateConvocation(value) {
    this.vm.fGroup.get('convocationTime').setValue(value._value);
  }
}
