import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoConvocationFormComponent } from './bo-convocation-form.component';

describe('BoConvocationFormComponent', () => {
  let component: BoConvocationFormComponent;
  let fixture: ComponentFixture<BoConvocationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoConvocationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoConvocationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
