import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

export class BoTeamFormComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-team-form',
  templateUrl: './bo-team-form.component.html',
  styleUrls: ['./bo-team-form.component.css']
})
export class BoTeamFormComponent implements OnInit {

  vm: BoTeamFormComponentVM = new BoTeamFormComponentVM();

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
