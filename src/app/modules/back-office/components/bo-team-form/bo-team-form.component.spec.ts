import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoTeamFormComponent } from './bo-team-form.component';

describe('BoTeamFormComponent', () => {
  let component: BoTeamFormComponent;
  let fixture: ComponentFixture<BoTeamFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoTeamFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoTeamFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
