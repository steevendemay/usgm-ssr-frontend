import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoSponsorFormComponent } from './bo-sponsor-form.component';

describe('BoSponsorFormComponent', () => {
  let component: BoSponsorFormComponent;
  let fixture: ComponentFixture<BoSponsorFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoSponsorFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoSponsorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
