import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {FormGroup} from '@angular/forms';
import {BoContactFormComponentVM} from '../bo-contact-form/bo-contact-form.component';

export class BoSponsorFormComponentVM {
  fGroup: FormGroup;

}

@Component({
  selector: 'app-bo-sponsor-form',
  templateUrl: './bo-sponsor-form.component.html',
  styleUrls: ['./bo-sponsor-form.component.css']
})
export class BoSponsorFormComponent implements OnInit {

  vm: BoSponsorFormComponentVM = new BoSponsorFormComponentVM();
  picture;
  faTimes = faTimes;


  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
    this.picture = this.vm.fGroup.get('picture').value;
  }

  @Input()
  set pic(value) {
    this.picture = value;
  }

  @Output() picEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }


  handleChangePicture(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture = (reader.result).toString();
      this.picEvent.emit(this.picture);
    };
  }

  handleClickDeletePicture(){
    this.picture = null;
    this.picEvent.emit(this.picture);
  }

}
