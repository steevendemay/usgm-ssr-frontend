import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Team} from '../../../shared-usgm/models/Team';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';

export class BoMatchFormComponentVM {
  fGroup: FormGroup;
  teams: Team[];
  dateMatch;
  dateReport;
}
@Component({
  selector: 'app-bo-match-form',
  templateUrl: './bo-match-form.component.html',
  styleUrls: ['./bo-match-form.component.css']
})
export class BoMatchFormComponent implements OnInit {

  vm: BoMatchFormComponentVM = new BoMatchFormComponentVM();

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
  }

  constructor(private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  getTeams() {
    this.boManager.getTeams().subscribe((teams) => {
      this.vm.teams = teams;
    });
  }

  handleChangeDateMatch(value) {
    this.vm.fGroup.get('dateMatch').setValue(value._value);
  }

  handleChangeDateReport(value) {
    this.vm.fGroup.get('reportDate').setValue(value._value);
  }

}
