import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoMatchFormComponent } from './bo-match-form.component';

describe('BoMatchFormComponent', () => {
  let component: BoMatchFormComponent;
  let fixture: ComponentFixture<BoMatchFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoMatchFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoMatchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
