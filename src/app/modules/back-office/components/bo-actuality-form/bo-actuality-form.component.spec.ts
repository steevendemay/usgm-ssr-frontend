import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoActualityFormComponent } from './bo-actuality-form.component';

describe('BoActualityFormComponent', () => {
  let component: BoActualityFormComponent;
  let fixture: ComponentFixture<BoActualityFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoActualityFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoActualityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
