import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {FormGroup} from '@angular/forms';

export class BoActualityFormComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-actuality-form',
  templateUrl: './bo-actuality-form.component.html',
  styleUrls: ['./bo-actuality-form.component.css']
})
export class BoActualityFormComponent implements OnInit {

  faTimes = faTimes;

  picture1;
  picture2;
  picture3;
  vm: BoActualityFormComponentVM = new BoActualityFormComponentVM();

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
    this.picture1 = this.vm.fGroup.get('picture1').value;
    this.picture2 = this.vm.fGroup.get('picture2').value;
    this.picture3 = this.vm.fGroup.get('picture3').value;
  }

  @Input()
  set pic1(value) {
    this.picture1 = value;
  }
  @Input()
  set pic2(value) {
    this.picture2 = value;
  }
  @Input()
  set pic3(value) {
    this.picture3 = value;
  }

  @Output() pic1Event: EventEmitter<any> = new EventEmitter();
  @Output() pic2Event: EventEmitter<any> = new EventEmitter();
  @Output() pic3Event: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  handleChangePicture1(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture1 = (reader.result).toString();
      this.pic1Event.emit(this.picture1);
    };
  }

  handleClickDeletePicture1(){
    this.picture1 = null;
    this.pic1Event.emit(this.picture1);
  }

  handleChangePicture2(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture2 = (reader.result).toString();
      this.pic2Event.emit(this.picture2);
    };
  }

  handleClickDeletePicture2(){
    this.picture2 = null;
    this.pic2Event.emit(this.picture2);
  }

  handleChangePicture3(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture3 = (reader.result).toString();
      this.pic3Event.emit(this.picture3);
    };
  }

  handleClickDeletePicture3(){
    this.picture3 = null;
    this.pic3Event.emit(this.picture3);
  }

}
