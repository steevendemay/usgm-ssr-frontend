import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../shared-usgm/models/Player';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {FormGroup} from '@angular/forms';
import {BackOfficeFormManagerService} from '../../services/back-office-form-manager.service';
import {Scorer} from '../../../shared-usgm/models/Scorer';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

export class BoScorerFormComponentVM {
  players: Player[];
  scorers: Scorer[];
  fGroup: FormGroup;
  faTimes = faTimes;
}
@Component({
  selector: 'app-bo-scorer-form',
  templateUrl: './bo-scorer-form.component.html',
  styleUrls: ['./bo-scorer-form.component.css']
})
export class BoScorerFormComponent implements OnInit {

  @Input() matchId: number;

  vm: BoScorerFormComponentVM = new BoScorerFormComponentVM();
  constructor(private boManager: BackOfficeManagerService,
              private boFormManager: BackOfficeFormManagerService) { }

  ngOnInit(): void {
    this.generateForm();
    this.getPlayers();
    this.getScorersByMatchId();
  }

  generateForm() {
    this.vm.fGroup = this.boFormManager.generateFormScorer();
  }

  getPlayers() {
    this.boManager.getPlayers().subscribe((players) => {
      this.vm.players = players;
    });
  }

  getScorersByMatchId() {
    this.boManager.getScorersByMatchId(this.matchId).subscribe(scorers => this.vm.scorers = scorers);
  }

  handleClickDeleteScorer(id: number) {
    this.boManager.deleteScorer(id).subscribe(() => this.getScorersByMatchId());
  }

  handleSubmitScorer() {
    this.vm.fGroup.get('matches_id').setValue(this.matchId);
    this.boManager.addScorer(this.vm.fGroup.value).subscribe(() => {
      this.getScorersByMatchId();
      this.vm.fGroup.reset();
    });
  }

}
