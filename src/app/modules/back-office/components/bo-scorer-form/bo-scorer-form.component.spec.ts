import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoScorerFormComponent } from './bo-scorer-form.component';

describe('BoScorerFormComponent', () => {
  let component: BoScorerFormComponent;
  let fixture: ComponentFixture<BoScorerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoScorerFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoScorerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
