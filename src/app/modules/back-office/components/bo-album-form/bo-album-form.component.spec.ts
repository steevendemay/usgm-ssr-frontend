import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoAlbumFormComponent } from './bo-album-form.component';

describe('BoAlbumFormComponent', () => {
  let component: BoAlbumFormComponent;
  let fixture: ComponentFixture<BoAlbumFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoAlbumFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoAlbumFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
