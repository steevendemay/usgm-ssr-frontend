import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Album} from '../../../shared-usgm/models/Album';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';

export class BoAlbumFormComponentVM {
  fGroup: FormGroup;
  albums: Album[];
}

@Component({
  selector: 'app-bo-album-form',
  templateUrl: './bo-album-form.component.html',
  styleUrls: ['./bo-album-form.component.css']
})
export class BoAlbumFormComponent implements OnInit {

  vm: BoAlbumFormComponentVM = new BoAlbumFormComponentVM();

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
  }

  constructor(private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getAlbums();
  }

  getAlbums() {
    this.boManager.getAlbums().subscribe((albums) => {
      this.vm.albums = albums;
    });
  }

}
