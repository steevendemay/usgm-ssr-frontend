import { Component, OnInit } from '@angular/core';
import {Team} from '../../../shared-usgm/models/Team';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {JwtManagerService} from '../../../shared-usgm/services/managers/jwt-manager.service';
import {faUser, faPowerOff} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {routesConstants} from '../../../shared-usgm/services/constants/routes';

export class BoMenuComponentVM {
  teams: Team[];
  nameUser: string;
  faUser = faUser;
  faPowerOff = faPowerOff;
}
@Component({
  selector: 'app-bo-menu',
  templateUrl: './bo-menu.component.html',
  styleUrls: ['./bo-menu.component.css']
})
export class BoMenuComponent implements OnInit {

  vm: BoMenuComponentVM = new BoMenuComponentVM();

  constructor(private boManager: BackOfficeManagerService,
              private jwtManager: JwtManagerService,
              private router: Router) { }

  ngOnInit(): void {
    this.getUserConnected();
    this.getTeams();
  }

  getTeams() {
    this.boManager.getTeams().subscribe((teams) => {
      this.vm.teams = teams;
    });
  }

  getUserConnected(){
    this.jwtManager.getUserConnected().subscribe((user) => {
      this.vm.nameUser = user.firstname;
      this.jwtManager.setFirstNameUserConnected(this.vm.nameUser);
    });
  }

  handleClickPublic() {
    this.router.navigate(['/']);
  }

  handleClickLogout() {
    this.jwtManager.storeJwt(null);
    this.jwtManager.setFirstNameUserConnected(null);
    this.vm.nameUser = null;
    this.router.navigate([routesConstants.foLogin]);
  }

}
