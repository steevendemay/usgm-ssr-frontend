import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoOfficeFormComponent } from './bo-office-form.component';

describe('BoOfficeFormComponent', () => {
  let component: BoOfficeFormComponent;
  let fixture: ComponentFixture<BoOfficeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoOfficeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoOfficeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
