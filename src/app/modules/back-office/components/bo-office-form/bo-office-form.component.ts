import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

export class BoOfficeFormComponentVM {
  fGroup: FormGroup;
}
@Component({
  selector: 'app-bo-office-form',
  templateUrl: './bo-office-form.component.html',
  styleUrls: ['./bo-office-form.component.css']
})
export class BoOfficeFormComponent implements OnInit {

  vm: BoOfficeFormComponentVM = new BoOfficeFormComponentVM();
  picture;
  faTimes = faTimes;


  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
    this.picture = this.vm.fGroup.get('picture').value;
  }

  @Input()
  set pic(value) {
    this.picture = value;
  }

  @Output() picEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  handleChangePicture(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture = (reader.result).toString();
      this.picEvent.emit(this.picture);
    };
  }

  handleClickDeletePicture(){
    this.picture = null;
    this.picEvent.emit(this.picture);
  }


}
