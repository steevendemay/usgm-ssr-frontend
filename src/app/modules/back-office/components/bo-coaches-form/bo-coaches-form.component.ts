import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BackOfficeManagerService} from '../../services/back-office-manager.service';
import {Team} from '../../../shared-usgm/models/Team';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

export class BoCoachesFormComponentVM {
  fGroup: FormGroup;
  teams: Team[];
}
@Component({
  selector: 'app-bo-coaches-form',
  templateUrl: './bo-coaches-form.component.html',
  styleUrls: ['./bo-coaches-form.component.css']
})
export class BoCoachesFormComponent implements OnInit {

  picture;
  faTimes = faTimes;
  vm: BoCoachesFormComponentVM = new BoCoachesFormComponentVM();

  @Input()
  set fGroup(value: FormGroup) {
    this.vm.fGroup = value;
    this.picture = this.vm.fGroup.get('picture').value;
  }

  @Input()
  set pic(value) {
    this.picture = value;
  }

  @Output() picEvent: EventEmitter<any> = new EventEmitter();

  constructor(private boManager: BackOfficeManagerService) { }

  ngOnInit(): void {
    this.getTeams();
  }


  getTeams() {
    this.boManager.getTeams().subscribe((teams) => {
      this.vm.teams = teams;
    });
  }

  handleChangePicture(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.picture = (reader.result).toString();
      this.picEvent.emit(this.picture);
    };
  }

  handleClickDeletePicture(){
    this.picture = null;
    this.picEvent.emit(this.picture);
  }

  handleClickDeleteDate() {
    this.vm.fGroup.get('birthdate').setValue(null);
  }

}
