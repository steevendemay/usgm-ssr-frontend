import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoCoachesFormComponent } from './bo-coaches-form.component';

describe('BoCoachesFormComponent', () => {
  let component: BoCoachesFormComponent;
  let fixture: ComponentFixture<BoCoachesFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoCoachesFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoCoachesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
