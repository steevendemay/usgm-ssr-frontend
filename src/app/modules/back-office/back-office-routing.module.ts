import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BoPlayersComponent} from './views/bo-players/bo-players.component';
import {BoAddPlayerComponent} from './views/bo-add-player/bo-add-player.component';
import {BoEditPlayerComponent} from './views/bo-edit-player/bo-edit-player.component';
import {BoEventsComponent} from './views/bo-events/bo-events.component';
import {BoAddEventComponent} from './views/bo-add-event/bo-add-event.component';
import {BoEditEventComponent} from './views/bo-edit-event/bo-edit-event.component';
import {routesConstants} from '../shared-usgm/services/constants/routes';
import {BoAddTeamComponent} from './views/bo-add-team/bo-add-team.component';
import {BoTeamsComponent} from './views/bo-teams/bo-teams.component';
import {BoEditTeamComponent} from './views/bo-edit-team/bo-edit-team.component';
import {MatchesComponent} from '../front-office/views/matches/matches.component';
import {BoMatchesComponent} from './views/bo-matches/bo-matches.component';
import {BoAddMatchComponent} from './views/bo-add-match/bo-add-match.component';
import {BoEditMatchComponent} from './views/bo-edit-match/bo-edit-match.component';
import {BoCoachesComponent} from './views/bo-coaches/bo-coaches.component';
import {BoAddCoachesComponent} from './views/bo-add-coaches/bo-add-coaches.component';
import {BoEditCoachesComponent} from './views/bo-edit-coaches/bo-edit-coaches.component';
import {BoOfficesComponent} from './views/bo-offices/bo-offices.component';
import {BoAddOfficeComponent} from './views/bo-add-office/bo-add-office.component';
import {BoEditOfficeComponent} from './views/bo-edit-office/bo-edit-office.component';
import {BoContactsComponent} from './views/bo-contacts/bo-contacts.component';
import {BoAddContactComponent} from './views/bo-add-contact/bo-add-contact.component';
import {BoEditContactComponent} from './views/bo-edit-contact/bo-edit-contact.component';
import {BoSponsorsComponent} from './views/bo-sponsors/bo-sponsors.component';
import {BoAddSponsorComponent} from './views/bo-add-sponsor/bo-add-sponsor.component';
import {BoEditSponsorComponent} from './views/bo-edit-sponsor/bo-edit-sponsor.component';
import {BoConvocationsComponent} from './views/bo-convocations/bo-convocations.component';
import {BoAddConvocationComponent} from './views/bo-add-convocation/bo-add-convocation.component';
import {BoEditConvocationComponent} from './views/bo-edit-convocation/bo-edit-convocation.component';
import {BoAddActualityComponent} from './views/bo-add-actuality/bo-add-actuality.component';
import {BoActualitiesComponent} from './views/bo-actualities/bo-actualities.component';
import {BoEditActualityComponent} from './views/bo-edit-actuality/bo-edit-actuality.component';
import {BoUsersComponent} from './views/bo-users/bo-users.component';
import {AuthGuard} from '../shared-usgm/guards/auth-guard';
import {BoAlbumsComponent} from './views/bo-albums/bo-albums.component';
import {BoAddAlbumComponent} from './views/bo-add-album/bo-add-album.component';
import {BoEditAlbumComponent} from './views/bo-edit-album/bo-edit-album.component';
import {BoPhotosAlbumComponent} from './views/bo-photos-album/bo-photos-album.component';


const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {path: routesConstants.boPlayers, component: BoPlayersComponent},
      {path: routesConstants.boAddPlayer, component: BoAddPlayerComponent},
      {path: routesConstants.boUpdatePlayer, component: BoEditPlayerComponent},
      {path: routesConstants.boUpdatePlayerWithoutParam, component: BoEditPlayerComponent},
      {path: routesConstants.boEvents, component: BoEventsComponent},
      {path: routesConstants.boAddEvent, component: BoAddEventComponent},
      {path: routesConstants.boUpdateEvent, component: BoEditEventComponent},
      {path: routesConstants.boUpdateEventWithoutParam, component: BoEditEventComponent},
      {path: routesConstants.boAddTeam, component: BoAddTeamComponent},
      {path: routesConstants.boTeams, component: BoTeamsComponent},
      {path: routesConstants.boUpdateTeamWithoutParam, component: BoEditTeamComponent},
      {path: routesConstants.boUpdateTeam, component: BoEditTeamComponent},

      {path: routesConstants.boMatchByTeam, component: BoMatchesComponent},
      {path: routesConstants.boAddMatch, component: BoAddMatchComponent},
      {path: routesConstants.boUpdateMatch, component: BoEditMatchComponent},
      {path: routesConstants.boUpdateMatchWithoutParam, component: BoEditMatchComponent},

      {path: routesConstants.boCoaches, component: BoCoachesComponent},
      {path: routesConstants.boAddCoach, component: BoAddCoachesComponent},
      {path: routesConstants.boUpdateCoach, component: BoEditCoachesComponent},
      {path: routesConstants.boUpdateCoachWithoutParam, component: BoEditCoachesComponent},

      {path: routesConstants.boOffices, component: BoOfficesComponent},
      {path: routesConstants.boAddOffice, component: BoAddOfficeComponent},
      {path: routesConstants.boUpdateOffice, component: BoEditOfficeComponent},
      {path: routesConstants.boUpdateOfficeWithoutParam, component: BoEditOfficeComponent},

      {path: routesConstants.boContacts, component: BoContactsComponent},
      {path: routesConstants.boAddContact, component: BoAddContactComponent},
      {path: routesConstants.boUpdateContact, component: BoEditContactComponent},
      {path: routesConstants.boUpdateContactWithoutParam, component: BoEditContactComponent},

      {path: routesConstants.boSponsors, component: BoSponsorsComponent},
      {path: routesConstants.boAddSponsor, component: BoAddSponsorComponent},
      {path: routesConstants.boUpdateSponsor, component: BoEditSponsorComponent},
      {path: routesConstants.boUpdateSponsorWithoutParam, component: BoEditSponsorComponent},


      {path: routesConstants.boConvocationByTeam, component: BoConvocationsComponent},
      {path: routesConstants.boAddConvocation, component: BoAddConvocationComponent},
      {path: routesConstants.boAddConvocationWithoutParam, component: BoAddConvocationComponent},
      {path: routesConstants.boUpdateConvocation, component: BoEditConvocationComponent},
      {path: routesConstants.boUpdateConvocationWithoutParam, component: BoEditConvocationComponent},

      {path: routesConstants.boAddActuality, component: BoAddActualityComponent},
      {path: routesConstants.boActualities, component: BoActualitiesComponent},
      {path: routesConstants.boUpdateActuality, component: BoEditActualityComponent},
      {path: routesConstants.boUpdateActualityWithoutParam, component: BoEditActualityComponent},

      {path: routesConstants.boUsers, component: BoUsersComponent},

      {path: routesConstants.boAlbums, component: BoAlbumsComponent},
      {path: routesConstants.boAddAlbum, component: BoAddAlbumComponent},
      {path: routesConstants.boUpdateAlbum, component: BoEditAlbumComponent},
      {path: routesConstants.boUpdateAlbumWithoutParam, component: BoEditAlbumComponent},


      {path: routesConstants.boPhotosAlbum, component: BoPhotosAlbumComponent},
      {path: routesConstants.boPhotosAlbumWithoutParam, component: BoPhotosAlbumComponent},

    ]
  }
];




@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class BackOfficeRoutingModule { }
